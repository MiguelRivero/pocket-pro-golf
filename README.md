# Pocket Pro Golf#

This is a computer game in Java made from Pocke Pro Golf Board Game.

More info about the game [here](http://www.boardgamegeek.com/boardgame/75809/pocket-pro-golf)

## Description ##

Pocket Pro Golf™ brings you the best aspects of real golf--club selection, careful aim, and shot consideration--in a pocket sized game that will fit in any golf bag.

One to four players can play anywhere from a single hole to a full round of 18 holes.

Unlike many other golf games, Pocket Pro Golf™ shows you the "narrative" of each shot as it works its way down the fairway. Watch as your ball comes perilously close to each hazard. Did you plan the shot well enough to keep your ball on course?

Each hole is created using a series of modular game cards. Hazards and features can be set up at random or arranged to resemble any hole from any golf course in the world.

Once the hole is assembled, players must choose the right club, aim carefully (taking wind into account) and hit the ball with the right swing tempo. Too fast and you'll lose control of the ball. Too slow and your ball won't go far enough.

The core of the game uses a clever movement mechanic that combines loss of control with distance. You use a number of dice to represent swing tempo (one two or three for slow, moderate and fast). Doubles and triples represent the effects of wind and slope, causing your ball to make semi-random deviations from a straight shot. Roll more dice and you're more likely to lose control and have your ball drift off course!