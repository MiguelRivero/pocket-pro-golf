package cards;


import java.awt.Point;
import java.io.IOException;

import org.junit.*;
import cards.HoleCard;

/**
 * @file TestHoleCard
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 1/1/2012
 * 
 * Class with test Junit of HoleCard class.
 */
public class TestHoleCard{
	
	/**
	 * Attribute holeCard.
	 */
	static HoleCard hc;
	static HoleCard hc2;
	
	
	/**
	 * @fn setUpBeforeClass().
	 * @throws IOException
	 * 
	 * Checks that HoleCard constructors are created.
	 */
	@Test
	public void setUpBeforeClass() throws IOException{
		hc=new HoleCard("files/green.txt");
		hc2 = new HoleCard("files/green.txt","G5");
	  
	}
	/**
	 * @fn void testHoleCardNameConstructor1() 
	 * @brief Test method for {@link cards.HoleCard#HoleCardName()}.
	 */
	@Test
	public void testHoleCardNameConstructor1(){
		Assert.assertTrue("HoleCard name incorrect","G1".equals(hc.getHoleCardName())
				||"G2".equals(hc.getHoleCardName()) ||"G3".equals(hc.getHoleCardName())
				||"G4".equals(hc.getHoleCardName())	||"G5".equals(hc.getHoleCardName())
				||"G6".equals(hc.getHoleCardName()) );
	}
	
	/**
	 * @fn void testHoleCardNameConstructor2().
	 * @brief Test method for {@link cards.HoleCard#HoleCardName()}.
	 */
	@Test
	public void testHoleCardNameConstructor2() {
		System.out.println(hc.toString());		
		Assert.assertTrue("HoleCard name incorrect", "G5".equals(hc2.getHoleCardName()));
	}
	
	/**
	 * @fn void testGetSurfaceException(). 
	 * @brief Test method for {@link cards.HoleCard#getSurface(java.awt.Point)}.
	 */
	@Test
	public void testGetSurfaceException(){
		Assert.assertTrue("HoleCard getSurface return error", "R".equals(hc.getSurface(new Point(6,3))) 
				|| "S".equals(hc.getSurface(new Point(6,3))) || "G".equals(hc.getSurface(new Point(6,3)))
				|| "N".equals(hc.getSurface(new Point(6,3))) || "W".equals(hc.getSurface(new Point(6,3))) 
				|| "TR".equals(hc.getSurface(new Point(6,3))));		
	}
	
	/**
	 * @fn void testToString(). 
	 * @brief Test method for {@link cards.HoleCard#getToString()}.
	 */
	@Test
	public void testToString(){
		Assert.assertTrue("HoleCard toString return null", hc.toString() != null);
		Assert.assertTrue("HoleCard toString return void string", !hc.toString().isEmpty());
	}
	/**
	 * @fn void testGetHoleSegment(). 
	 * @brief Test method for {@link cards.HoleCard#getHoleSegment()}.
	 * @throws IOException
	 */
	@Test
	public void testGetHoleSegment() throws IOException{
		Assert.assertTrue("HoleSegment is incorrect",hc.getHoleSegment().equals(hc.getHoleSegment()));
	
	}
	/**
	 * @fn void testGetRebound()
	 * @brief Test method for {@link cards.HoleCard#getRebound()}.
	 */
	@Test
	public void testGetRebound(){
		Point point = new Point(0,0);
		Assert.assertTrue("Coordinate x is 0",point.getX() ==0);
		Assert.assertTrue("Coordinate y is 0", point.getY()==0);		
	}
}
