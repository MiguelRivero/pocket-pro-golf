package cards;

import static org.junit.Assert.*;
import java.io.IOException;
import org.junit.Test;

/**
 * @file TestFairway
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 1/1/2012
 * 
 * Class of Junit test Fairway class.
 */
public class TestFairway{

	/**
	 * @fn void testFairway(). 
	 * @brief Test method for {@link cards.Fairway#Fairway()}.
	 * @throws IOException
	 */
	@Test
	public void testFairway() throws IOException{
		Fairway fw = new Fairway();
		String txt = "files/fairway.txt";
		
		HoleCard fw1 = new HoleCard(txt,"F1");
		HoleCard fw2 = new HoleCard(txt,"F2");
		HoleCard fw3 = new HoleCard(txt,"F3");
		HoleCard fw4 = new HoleCard(txt,"F4");
		HoleCard fw5 = new HoleCard(txt,"F5");
		HoleCard fw6 = new HoleCard(txt,"F6");
		HoleCard fw7 = new HoleCard(txt,"F7");
		HoleCard fw8 = new HoleCard(txt,"F8");
		HoleCard fw9 = new HoleCard(txt,"F9");
		HoleCard fw10 = new HoleCard(txt,"F10");
		HoleCard fw11 = new HoleCard(txt,"F11");
		HoleCard fw12 = new HoleCard(txt,"F12");
		HoleCard fw13 = new HoleCard(txt,"F13");
		HoleCard fw14 = new HoleCard(txt,"F14");
		
		assertTrue("Error in constructor.", fw.toString().equals(fw1.toString())||
				fw.toString().equals(fw2.toString())||fw.toString().equals(fw3.toString())||
				fw.toString().equals(fw4.toString())||fw.toString().equals(fw5.toString())||
				fw.toString().equals(fw6.toString())||fw.toString().equals(fw7.toString())||
				fw.toString().equals(fw8.toString())||fw.toString().equals(fw9.toString())||
				fw.toString().equals(fw10.toString())||fw.toString().equals(fw11.toString())||
				fw.toString().equals(fw12.toString())||fw.toString().equals(fw13.toString())||
				fw.toString().equals(fw14.toString()));
	}

	/**
	 * @fn void testFairwayCard(). 
	 * @brief Test method for {@link cards.Fairway#Fairway(java.lang.String)}.
	 * @throws IOException
	 */
	@Test
	public void testFairwayCard() throws IOException{
		Fairway fw1 = new Fairway("F2");
		HoleCard fw2 = new HoleCard("files/fairway.txt", "F2");
		assertTrue("Error in constructor.",fw1.toString().equals(fw2.toString()));
	}
}
