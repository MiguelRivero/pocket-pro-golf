package cards;

import static org.junit.Assert.*;
import java.awt.Point;
import java.io.IOException;
import org.junit.Test;

/**
 * @file TestTee
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 1/1/2012
 * 
 * Class with test Junit of Tee class.
 */
public class TestTee{

	/**
	 * @fn void testTee(). 
	 * @brief Test method for {@link cards.Tee#Tee()}.
	 * @throws IOException
	 */
	@Test
	public void testTee() throws IOException {
		Tee t=  new Tee();
		String txt = "files/tee.txt";
		
		HoleCard t1 = new HoleCard(txt,"T1");
		HoleCard t2 = new HoleCard(txt,"T2");
		HoleCard t3 = new HoleCard(txt,"T3");
		assertTrue("Error", t.toString().equals(t1.toString()) 
				|| t.toString().equals(t2.toString())
				|| t.toString().equals(t3.toString()));		
	}

	/**
	 * @fn void testTeeCard(). 
	 * @brief Test method for {@link cards.Tee#Tee(java.lang.String)}.
	 * @throws IOException
	 */
	@Test
	public void testTeeCard() throws IOException {
		Tee t= new Tee("T3");
		HoleCard t3= new HoleCard("files/tee.txt","T3");
		assertTrue("El constructor concreto no funciona correctamente", t.toString().equals(t3.toString()));
	}

	/**
	 * @fn void testGetStartPoint().
	 * @brief Test method for {@link cards.Tee#getStartPoint()}.
	 * @throws IOException.
	 */
	@Test
	public void testGetStartPoint() throws IOException {
		Tee t= new Tee("T1");
		Point p= new Point(3, 0);
		assertTrue("Error en el Punto de comienzo", t.getStartPoint().equals(p));
	}
}
