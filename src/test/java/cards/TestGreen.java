package cards;

import static org.junit.Assert.*;
import java.awt.Point;
import java.io.IOException;
import org.junit.Test;

/**
 * @file TestGreen
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 1/1/2012
 * 
 * Class with test Junit of Green class.
 */
public class TestGreen{

	/**
	 * @fn void testGreen().
	 * @brief Test method for {@link cards.Green#Green()}.
	 * @throws IOException 
	 */
	@Test
	public void testGreen() throws IOException{
		Green g= new Green();
		String txt = "files/green.txt";
		
		HoleCard g1 = new HoleCard(txt,"G1");
		HoleCard g2 = new HoleCard(txt,"G2");
		HoleCard g3 = new HoleCard(txt,"G3");
		HoleCard g4 = new HoleCard(txt,"G4");
		HoleCard g5 = new HoleCard(txt,"G5");
		HoleCard g6 = new HoleCard(txt,"G6");
		
		assertTrue("Error in constructor.", 
				g.toString().equals(g1.toString()) || g.toString().equals(g2.toString()) ||
				g.toString().equals(g3.toString()) || g.toString().equals(g4.toString()) || 
				g.toString().equals(g5.toString()) || g.toString().equals(g6.toString()) );
	}

	/**
	 * @fn void testGreenCard().
	 * @brief Test method for {@link cards.Green#Green(java.lang.String)}.
	 * @throws IOException 
	 */
	@Test
	public void testGreenCard() throws IOException{
		Green g= new Green("G3");
		HoleCard hc= new HoleCard("files/green.txt", "G3");
		assertTrue("Error in constructor.", g.toString().equals(hc.toString()));
	}

	/**
	 * @fn void testIsNextToHole().
	 * @brief Test method for {@link cards.Green#isNextToHole(java.awt.Point)}.
	 * @throws IOException 
	 */
	@Test
	public void testIsNextToHole() throws IOException{
		Green g = new Green("G3");
		Point p = new Point(3,4);
		assertTrue("Error in method IsNextToHole.", g.isNextToHole(p));
	}
}
