package utilities;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * @file TestSurfaces
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 1/1/2012
 * 
 * Class of Junit test Surfaces class.
 */
public class TestSurfaces {

	/**
	 * @throws java.lang.Exception
	 */		
	@Test
	public void testGetRough() {		
		assertTrue("Error", Surfaces.getRough().equals("R"));
	}

	/**
	 * Test method for {@link utilities.Surfaces#getFairway()}.
	 */
	@Test
	public void testGetFairway() {		
		assertTrue("Error", Surfaces.getFairway().equals("F"));
	}

	/**
	 * Test method for {@link utilities.Surfaces#getTee()}.
	 */
	@Test
	public void testGetTee() {		
		assertTrue("Error", Surfaces.getTee().equals("T"));
	}

	/**
	 * Test method for {@link utilities.Surfaces#getSand()}.
	 */
	@Test
	public void testGetSand() {
		assertTrue("Error", Surfaces.getSand().equals("S"));
	}

	/**
	 * Test method for {@link utilities.Surfaces#getGreen()}.
	 */
	@Test
	public void testGetGreen() {
		assertTrue("Error", Surfaces.getGreen().equals("G"));
	}

	/**
	 * Test method for {@link utilities.Surfaces#getNextToHole()}.
	 */
	@Test
	public void testGetNextToHole() {
		assertTrue("Error", Surfaces.getNextToHole().equals("N"));
	}

	/**
	 * Test method for {@link utilities.Surfaces#getTree()}.
	 */
	@Test
	public void testGetTree() {
		assertTrue("Error", Surfaces.getTree().equals("TR"));;
	}

	/**
	 * Test method for {@link utilities.Surfaces#getWater()}.
	 */
	@Test
	public void testGetWater() {
		assertTrue("Error", Surfaces.getWater().equals("W"));
	}

	/**
	 * Test method for {@link utilities.Surfaces#getRebound()}.
	 */
	@Test
	public void testGetRebound() {
		assertTrue("Error", Surfaces.getRebound().equals("RT"));
	}
}
