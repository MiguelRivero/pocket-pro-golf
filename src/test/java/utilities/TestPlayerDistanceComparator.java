package utilities;

import java.awt.Point;

import junit.framework.Assert;

import models.Player;

import org.junit.BeforeClass;
import org.junit.Test;

public class TestPlayerDistanceComparator {
	
	
	/**
	 * Attribute Player player.
	 */
	static Player player;
	/**
	 * Attribute Point pointA.
	 */
	static Point pointA;
	/**
	 * Attribute Point pointB.
	 */
	static Point pointB;
	/**
	 * Attribute Double distToA.
	 */
	static Double distToA;
	/**
	 * Attribute Double distToB.
	 */
	static Double distToB;
	
	
	/**
	 * @fn setUpBeforeClass()
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		player = new Player("Tiger Woods");
		player.setBallPosition(new Point(3,5));
		
		pointA = new Point(4,6);
		pointB = new Point(7,9);
		
		distToA = player.getBallPosition().distance(pointA);
		distToB = player.getBallPosition().distance(pointB);
		
	}
	
	@Test
	public void testCompareDistances() {
		
		Assert.assertTrue("Error in distances", distToA.compareTo(distToB)<0);
		
	}

}
