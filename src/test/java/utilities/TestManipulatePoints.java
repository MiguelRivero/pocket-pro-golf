package utilities;

import static org.junit.Assert.*;
import java.awt.Point;
import org.junit.*;

/**
 * @file TestManipulatePoints
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 1/1/2012
 * 
 * Class of Junit test ManipulatePoints class.
 */
public class TestManipulatePoints {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link utilities.ManipulatePoints#sumPoints(java.awt.Point, java.awt.Point)}.
	 */
	@Test
	public void testSumPoints() {
		Point a=new Point(4, 2);
		Point b=new Point(1, 6);
		assertTrue("Wrong sum", ManipulatePoints.sumPoints(a, b).equals(new Point(5, 8)));
	}

	/**
	 * Test method for {@link utilities.ManipulatePoints#reversePoint(java.awt.Point)}.
	 */
	@Test
	public void testReversePoint() {
		Point a=new Point(4, 2);
		assertTrue("Wrong reversePoint", ManipulatePoints.reversePoint(a).equals(new Point(-4, -2)));
	}
}
