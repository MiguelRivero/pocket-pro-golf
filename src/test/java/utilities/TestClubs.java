/**
 * 
 */
package utilities;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * @file TestClubs.
 * @author Grupo 14, ISG2.
 * @date 11/01/2012
 * @version 0.1
 * 
 * Test of class Clubs
 */
public class TestClubs {

	 /**
	  * @fn testGetDriver()
	  * 
	  * Checks that driver is correct.
	  */
	@Test
	public void testGetDriver() {
		assertTrue("Error", Clubs.getDriver().equals("Driver"));
	}

	/**
	 * @fn testGetWedge()
	 * 
	 * Checks that Wedge is correct.
	 */
	@Test
	public void testGetWedge() {
		assertTrue("Error", Clubs.getWedge().equals("Wedge"));
	}

	/**
	 * @fn testGetIron3()
	 * 
	 * Checks that 3 Iron is correct. 
	 */
	@Test
	public void testGetIron3() {
		assertTrue("Error", Clubs.getIron3().equals("3Iron"));
	}

	/**
	 * @fn testGetIron5()
	 *
	 * Checks that 5 Iron is correct.
	 */
	@Test
	public void testGetIron5() {
		assertTrue("Error", Clubs.getIron5().equals("5Iron"));
	}

	/**
	 * @fn testGetIron7()
	 * 
	 * Checks that 7 Iron is correct.
	 */
	@Test
	public void testGetIron7() {
		assertTrue("Error", Clubs.getIron7().equals("7Iron"));
	}

	/**
	 * @fn testGetIron9()
	 * 
	 * Checks that 9 Iron is correct.
	 */
	@Test
	public void testGetIron9() {
		assertTrue("Error", Clubs.getIron9().equals("9Iron"));
	}

	/**
	 * @fn testGetPutter()
	 * 
	 * Checks that Putter is correct.
	 */
	@Test
	public void testGetPutter() {
		assertTrue("Error", Clubs.getPutter().equals("Putter"));
	}
}
