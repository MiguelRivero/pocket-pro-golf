package mains;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.*;

import org.junit.*;

import utilities.*;

/**
 * @file TestThrow
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 4/1/2012
 * 
 * Class of Junit test of Throw  class.
 */
public class TestThrow {

	/**
     *  Attribute valuesTest.
     */
	private static Map<Integer,Integer> valuesTest;
	
	/**
	 * @fn setUpBeforeClass()
	 * @throws IOException
	 */
	@Test
	public void setUpBeforeClass() throws IOException{
		valuesTest = new HashMap<Integer, Integer>();
		InputStream fe = new InputStream();
				
		try{
			fe = new InputStream("files/valuesThrow.txt");
		}catch(IOException e){
			e.printStackTrace();
		}
				
		ArrayList<String> lines = new ArrayList<String>();
		for (String line : fe) {
			lines.add(line);
		}
		for(String v:lines){
			ArrayList<String> val = Strings.separateElements(v, ":");
			valuesTest.put(Integer.valueOf(val.get(0)),Integer.valueOf(val.get(1)));
		}
	}

	/**
	 * @fn void testThrow().
	 * @brief Test method for {@link mains.Throw#Throw()}.
	 */
	@Test
	public void testThrow(){
		Throw th= new Throw();
		assertTrue("Dado1 mal iniciado", th.getDice1()==0);
		assertTrue("Dado2 mal iniciado", th.getDice2()==0);
		assertTrue("Dado3 mal iniciado", th.getDice3()==0);
		assertTrue("progress mal iniciado", th.getProgress()==0);
		assertTrue("rollDoubles mal iniciado", th.getRollDoubles()==false);
		assertTrue("rollTriples mal iniciado", th.getRollTriples()==false);
		assertTrue("Carta Values mal cargada", valuesTest.equals(th.getValues()));
	}
	
	/**
	 * @fn void testGetDice1().
	 * @brief Test method for {@link mains.Throw#getDice1()}.
	 */
	@Test
	public void testGetDice1() {
		Throw th= new Throw();
		assertTrue("Error getting Dice1", th.getDice1()==0);
	}

	/**
	 * @fn void testGetDice2().
	 * @brief Test method for {@link mains.Throw#getDice2()}.
	 */
	@Test
	public void testGetDice2() {
		Throw th= new Throw();
		assertTrue("Error getting Dice1", th.getDice2()==0);
	}

	/**
	 * @fn void testGetDice3().
	 * @brief Test method for {@link mains.Throw#getDice3()}.
	 */
	@Test
	public void testGetDice3() {
		Throw th= new Throw();
		assertTrue("Error getting Dice1", th.getDice3()==0);
	}	

	/**
	 * @fn void testSetDice1().
	 * @brief Test method for {@link mains.Throw#setDice1()}.
	 */
	@Test
	public void testSetDice1() {
		Throw th= new Throw();
		th.setDice1(4);
		assertTrue("Error getting Dice1", th.getDice1()==4);
	}

	/**
	 * @fn void testSetDice2().
	 * @brief Test method for {@link mains.Throw#setDice2()}.
	 */
	@Test
	public void testSetDice2() {
		Throw th= new Throw();
		th.setDice2(5);
		assertTrue("Error getting Dice1", th.getDice2()==5);
	}

	/**
	 * @fn void testSetDice3().
	 * @brief Test method for {@link mains.Throw#setDice3()}.
	 */
	@Test
	public void testSetDice3() {
		Throw th= new Throw();
		th.setDice3(2);
		assertTrue("Error getting Dice1", th.getDice3()==2);
	}
	
	/**
	 * @fn void testGetProgress1()
	 * @brief Test method for {@link mains.Throw#getProgress()}.
	 */
	@Test
	public void testGetProgress1(){
		Throw th= new Throw();
		assertTrue("Error in getProgress",th.getProgress()==0);
	}
	
	/**
	 * @fn void testGetProgress2()
	 * @brief Test method for {@link mains.Throw#getProgress()}.
	 */
	@Test
	public void testGetProgress2(){
		Throw th= new Throw();
		th.fastThrow();
		assertTrue("Error in getProgress",th.getProgress()>0);
	}
	
	/**
	 * @fn void testSetProgress()
	 * @brief Test method for {@link mains.Throw#setProgress(Integer)}.
	 */
	@Test
	public void testSetProgress(){
		Throw th= new Throw();
		th.fastThrow();
		th.setProgress(7);
		assertTrue("Error in setProgress",th.getProgress()==7);
	}
	
	/**
	 * @fn void testGetRollDoubles().
	 * @brief Test method for {@link mains.Throw#getRollDoubles()}.
	 */
	@Test
	public void testGetRollDoubles(){
		Throw th= new Throw();
		assertTrue("Error in getRollDoubles", th.getRollDoubles().equals(false));
	}

	/**
	 * @fn testGetRollTriples()
	 * @brief Test method for {@link mains.Throw#getRollTriples()}.
	 */
	@Test
	public void testGetRollTriples(){
		Throw th= new Throw();
		assertTrue("Error in getRollTrilples", th.getRollTriples().equals(false));
	}
	
	/**
	 * @fn void testSetRollDoubles().
	 * @brief Test method for {@link mains.Throw#setRollDoubles(java.lang.Boolean)}.
	 */
	@Test
	public void testSetRollDoubles() {
		Throw th= new Throw();
		th.setRollDoubles(true);
		assertTrue("Error setting RollDoubles", th.getRollDoubles().equals(true));
	}

	/**
	 * @fn testSetRollTriples().
	 * @brief Test method for {@link mains.Throw#setRollTriples(java.lang.Boolean)}.
	 */
	@Test
	public void testSetRollTriples() {
		Throw th= new Throw();
		th.setRollTriples(true);
		assertTrue("Error setting RollDoubles", th.getRollTriples().equals(true));
	}
	
	/**
	 * @fn void testResetRolls1().
	 * @brief Test method for {@link mains.Throw#resetRolls()}.
	 */
	@Test
	public void testResetRolls1(){
		Throw th= new Throw();
		th.fastThrow();
		th.setRollDoubles(true);
		th.setRollTriples(true);
		th.resetRolls();
		assertTrue("Error in ResetRoll.", th.getRollDoubles().equals(false) && th.getRollTriples().equals(false) && th.getDice1().equals(0) && th.getDice2().equals(0)	&& th.getDice3().equals(0));		
	}
	
	/**
	 * @fn void testResetRolls2()
	 * @brief Test method for {@link mains.Throw#resetRolls()}.
	 */
	@Test
	public void testResetRolls2(){
		Throw th= new Throw();
		th.normalThrow();
		th.setRollDoubles(true);
		th.setRollTriples(false);
		th.resetRolls();
		assertTrue("Error in ResetRoll.", th.getRollDoubles().equals(false) && th.getRollTriples().equals(false) && th.getDice1().equals(0) && th.getDice2().equals(0)	&& th.getDice3().equals(0));		
	}
	
	/**
	 * @fn void testResetRolls3()
	 * @brief Test method for {@link mains.Throw#resetRolls()}.
	 */
	@Test
	public void testResetRolls3(){
		Throw th= new Throw();
		th.setRollTriples(true);
		th.resetRolls();
		assertTrue("Error in ResetRoll.", th.getRollDoubles().equals(false) && th.getRollTriples().equals(false));		
	}	
	
	/**
	 * @fn void testGetValues()
	 * @brief Test method for {@link mains.Throw#getValues()}.
	 */
	@Test
    public void testGetValues(){
		Throw th= new Throw();
		assertTrue("Error in getValues.", valuesTest.equals(th.getValues()));
	}
	
	/**
	 * @fn void testThrowDices1()
	 * @brief Test method for {@link mains.Throw#throwDices(java.lang.Integer)}.
	 */
	@Test
	public void testThrowDices1() {
		Throw th= new Throw();
		th.throwDices(0);
		assertTrue("Error throwing dices with swingTempo = 0", th.getProgress()==1);		
	}
	
	/**
	 * @fn void testThrowDices2()
	 * @brief Test method for {@link mains.Throw#throwDices(java.lang.Integer)}.
	 */
	@Test
	public void testThrowDices2() {
		Throw th= new Throw();
		th.throwDices(1);
		assertTrue("Error throwing dices with swingTempo = 1", th.getDice1()>0 && th.getDice2()==0 && th.getDice3()==0);
		assertTrue("Error throwing dices with swingTempo = 1", th.getRollDoubles().equals(false) && th.getRollTriples().equals(false));		
	}
	/**
	 * @fn void testThrowDices3()
	 * @brief Test method for {@link mains.Throw#throwDices(java.lang.Integer)}.
	 */
	@Test
	public void testThrowDices3() {
		Throw th= new Throw();
		th.throwDices(2);
		assertTrue("Error throwing dices with swingTempo = 2", th.getDice1()>0 && th.getDice2()>0 && th.getDice3()==0);
		assertTrue("Error throwing dices with swingTempo = 2", th.getRollTriples().equals(false));		
	}
	
	/**
	 * @fn void testThrowDices4()
	 * @brief Test method for {@link mains.Throw#throwDices(java.lang.Integer)}.
	 */
	@Test
	public void testThrowDices4() {
		Throw th= new Throw();
		th.throwDices(3);
		assertTrue("Error throwing dices with swingTempo = 3", th.getDice1()>0 && th.getDice2()>0 && th.getDice3()>0);
		
	}
	
	/**
	 * @fn void testNonThrow1()
	 * @brief Test method for {@link mains.Throw#nonThrow()}.
	 */
	@Test
	public void testNonThrow1(){
		Throw th= new Throw();
		th.nonThrow();
		assertTrue("Incorrect progress.", th.getProgress()==1);
	}
	
	/**
	 * @fn void testSlowThrow()
	 * @brief Test method for {@link mains.Throw#slowThrow()}.
	 */
	@Test
	public void testSlowThrow(){
		Throw th= new Throw();
		th.slowThrow();
		assertTrue("Error throwing dices with swingTempo = 1", th.getDice1()>0 && th.getDice2()==0 && th.getDice3()==0);
	}

	/**
	 * @fn void testNormalThrow()
	 * @brief Test method for {@link mains.Throw#normalThrow()}.
	 */
	@Test
	public void testNormalThrow(){
		Throw th= new Throw();
		th.normalThrow();
		assertTrue("Error throwing dices with swingTempo = 2", th.getDice1()>0 && th.getDice2()>0 && th.getDice3()==0);
	}

	/**
	 * @fn void testFastThrow()
	 * @brief Test method for {@link mains.Throw#fastThrow()}.
	 */
	@Test
	public void testFastThrow(){
		Throw th= new Throw();
		th.fastThrow();
		assertTrue("Error throwing dices with swingTempo = 3", th.getDice1()>0 && th.getDice2()>0 && th.getDice3()>0);
	}

	/**
	 * @fn void testIsDouble()
	 * @brief Test method for {@link mains.Throw#isDouble(java.lang.Integer, java.lang.Integer)}.
	 */
	@Test
	public void testIsDouble(){
		assertTrue("Error in isDouble", Throw.isDouble(3, 3).equals(true));
	}

	/**
	 * @fn void testIsTriple()
	 * @brief Test method for {@link mains.Throw#isTriple(java.lang.Integer, java.lang.Integer, java.lang.Integer)}.
	 */
	@Test
	public void testIsTriple(){
		assertTrue("Error in isDouble", Throw.isTriple(4, 4, 4).equals(true));
	}	

	/**
	 * @fn void testIsRollEight1()
	 * @brief Test method for {@link mains.Throw#isRollEight()}.
	 */
	@Test
	public void testIsRollEight1(){
		Throw th= new Throw();
		th.setProgress(7);
		th.setRollDoubles(false);
		assertTrue("Error in isRollEight.", th.isRollEight().equals(false));
	}
	
	/**
	 * @fn void testIsRollEight2().
	 * @brief Test method for {@link mains.Throw#isRollEight()}.
	 */
	@Test
	public void testIsRollEight2(){
		Throw th= new Throw();
		th.setProgress(8);
		th.setRollDoubles(true);
		assertTrue("Error in isRollEight.", th.isRollEight().equals(false));
	}
	
	/**
	 * @fn void testIsRollEight3()
	 * @brief Test method for {@link mains.Throw#isRollEight()}.
	 */
	@Test
	public void testIsRollEight3(){
		Throw th= new Throw();
		th.setDice1(1);
		th.setDice2(3);
		th.setDice3(4);
		th.setRollDoubles(false);
		th.setRollTriples(false);
		assertTrue("Error in isRollEight", th.isRollEight().equals(true));
	}
	
	/**
	 * @fn void testValues().
	 * @brief Test method for values.
	 */
	@Test
	public void testValues(){
		Throw th= new Throw();
		th.fastThrow();
		if(th.getRollDoubles() || th.getRollTriples())
			assertTrue("No funciona correctamente0",th.getProgress()==2);
		else{ 
			if((th.getDice1()+th.getDice2()+th.getDice3())>=1 && (th.getDice1()+th.getDice2()+th.getDice3())<6)
				assertTrue("No funciona correctamente1",th.getProgress()==1);
			else if ((th.getDice1()+th.getDice2()+th.getDice3())>=6 && (th.getDice1()+th.getDice2()+th.getDice3())<11)
				assertTrue("No funciona correctamente2",th.getProgress()==2);
			else if ((th.getDice1()+th.getDice2()+th.getDice3())>=11 && (th.getDice1()+th.getDice2()+th.getDice3())<14)
				assertTrue("No funciona correctamente3",th.getProgress()==3);
			else if ((th.getDice1()+th.getDice2()+th.getDice3())==14 || (th.getDice1()+th.getDice2()+th.getDice3())==15)
				assertTrue("No funciona correctamente4",th.getProgress()==4);
		}
	}	
}
