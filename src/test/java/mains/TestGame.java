package mains;

import java.awt.Point;
import java.io.*;
import java.lang.reflect.*;
import java.util.*;
import models.*;
import org.junit.*;

import utilities.Surfaces;

/**
 * @file TestGame
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 3/1/2012
 * 
 * Class with Junit test of Game class.
 */
public class TestGame {

	/**
	 * Attribute game
	 */
	static Game game;
	static Hole hole;

	/**
	 * @fn setUpBeforeClass
	 * @brief Creates a default game.
	 * @throws IOException
	 */
	@Test
	public void setUpBeforeClass() throws IOException {
		String tournamentName = "Glorio's Open";
		Player myPlayer = new Player("Glorio");
		List<Player> playerList = new ArrayList<Player>();
		playerList.add(myPlayer);
		List<String> listCardNames = new ArrayList<String>();
		listCardNames.add("T3");
		listCardNames.add("G4");
		Hole hole = new Hole(listCardNames);
		List<Hole> listHole = new ArrayList<Hole>();
		listHole.add(hole);
		Tournament tour = new Tournament(tournamentName, playerList, listHole);
		game = new Game(tour);
	}

	/**
	 * @fn tearDownAfterClass
	 * @throws IOException
	 */
	@Test
	public void tearDownAfterClass() throws IOException {
	}

	/**
	 * @fn testCalculateDirectionLeft()
	 * 
	 * Checks out that the direction of the ball will go to the left.
	 */
	@Test
	public void testCalculateDirectionLeft() {
		Point from = new Point(6, 14);
		Point to = new Point(4, 15);
		Object[] params = { from, to };
		Point direction = (Point) executePrivateMethod(game,"calculateDirection", params);
		Assert.assertTrue("This is not the wanted direction", direction.x == -1 && direction.y == 0);
	}

	/**
	 * @fn testCalculateDirectionRight()
	 * 
	 * Checks out that the direction of the ball will go to the right
	 */
	@Test
	public void testCalculateDirectionRight() {
		Point from = new Point(0, 14);
		Point to = new Point(3, 15);
		Object[] params = { from, to };
		Point direction = (Point) executePrivateMethod(game, "calculateDirection", params);
		Assert.assertTrue("This is not the wanted direction", direction.x == 1 && direction.y == 0);
	}

	/**
	 * @fn testCalculateDirectionUp()
	 * 
	 * Checks out that the direction of the ball will go up
	 */
	@Test
	public void testCalculateDirectionUp() {
		Point from = new Point(0, 0);
		Point to = new Point(4, 15);
		Object[] params = { from, to };
		Point direction = (Point) executePrivateMethod(game,
				"calculateDirection", params);
		Assert.assertTrue("This is not the wanted direction", direction.x == 0
				&& direction.y == 1);
	}

	/**
	 * @fn testCalculateDirectionDown()
	 * 
	 * Checks out that the direction of the ball will go down
	 */
	@Test
	public void testCalculateDirectionDown() {
		Point from = new Point(3, 17);
		Point to = new Point(3, 15);
		// game.calculateDirection(from, to);
		Object[] params = { from, to };
		Point direction = (Point) executePrivateMethod(game,
				"calculateDirection", params);
		Assert.assertTrue("This is not the wanted direction", direction.x == 0
				&& direction.y == -1);
	}
	
    /**
     * @fn testCalculateAimRight()
     * 
     * Checks that the calculate Aim for Right is correct.
     */
	@Test
	public void testCalculateAimRight() {
		int dirX = 0, dirY = 1;
		int aimX = -2, aimY = 0;
		Point direction = new Point(dirX, dirY);
		Point aim = new Point(aimX, aimY);
		Object [] params = {direction, aim};
		Point resultAim = (Point) executePrivateMethod(game, "calculateAim", params);
		Assert.assertTrue("This is not the wanted aim", resultAim.x == aimX && resultAim.y == aimY);
	}
	
	/**
	 * @fn testcalculateAimLeft()
	 * 
	 * Checks that the calculate Aim for Left is correct.
	 */
	@Test
	public void testcalculateAimLeft() {
		int dirX = 0, dirY = -1;
		int aimX = -2, aimY = 0;
		Point direction = new Point(dirX, dirY);
		Point aim = new Point(aimX, aimY);
		Object [] params = {direction, aim};
		Point resultAim = (Point) executePrivateMethod(game, "calculateAim", params);
		Assert.assertTrue("This is not the wanted aim", resultAim.x == -aimX && resultAim.y == aimY);
	}
	
	/**
	 * @fn testcalculateAimUp()
	 * 
	 * Checks that the calculate of Aim for Up is correct.
	 */
	@Test
	public void testcalculateAimUp() {
		int dirX = 1, dirY = 0;
		int aimX = -2, aimY = 0;
		Point direction = new Point(dirX, dirY);
		Point aim = new Point(aimX, aimY);
		Object [] params = {direction, aim};
		Point resultAim = (Point) executePrivateMethod(game, "calculateAim", params);
		Assert.assertTrue("This is not the wanted aim", resultAim.x == aimY && resultAim.y == -aimX);
	}
	
	/**
	 * testcalculateAimDown()
	 * 
	 * Checks that the calculate Aim for Down is correct.
	 */
	@Test
	public void testcalculateAimDown() {
		int dirX = -1, dirY = 0;
		int aimX = -2, aimY = 0;
		Point direction = new Point(dirX, dirY);
		Point aim = new Point(aimX, aimY);
		Object [] params = {direction, aim};
		Point resultAim = (Point) executePrivateMethod(game, "calculateAim", params);
		Assert.assertTrue("This is not the wanted aim", resultAim.x == aimY && resultAim.y == aimX);
	}
	
	@Test
	public void testCalculateCurrentPlayer(){
		Game g = newGame(create4Players());
		Object [] params = {hole};
		Player player = (Player)executePrivateMethod(g, "calculateCurrentPlayer", params);
		Point point = player.getBallPosition();
		Assert.assertTrue("This is not the player spected", point.x == 0 && point.y == 0);
	}
	
	@Test
	public void testIsHoleEnded(){
		Game g = newGame(create4PlayersNextToHole(hole.getNextToHoleList()));
		Object[] params = {hole};
		Boolean end = (Boolean)executePrivateMethod(g, "isHoleEnded", params);
		Assert.assertTrue("The hole is not ended yet", end);
	}
	
	@Test
	public void testIsThisSurface(){
		Object[] params = {hole, new Point(1,12), Surfaces.getWater()};
		Boolean result = (Boolean)executePrivateMethod(game, "isThisSurface", params);
		Assert.assertTrue("The hole is not ended yet", result);
	}
	
	/**
	 * @fn executePrivateMethod(Object obj, String methodName, Objects[] params)
	 * @param obj
	 * @param methodName
	 * @param params
	 * @return Object
	 */
	public Object executePrivateMethod(Object obj, String methodName, Object[] params) {
		Object o = null;
		Boolean flag = false;
		// using reflection to ask the object for its methods
		Method[] methods = obj.getClass().getDeclaredMethods();
		Method method = null;
		// iterating through the methods to find the one we're looking for
		for (int i = 0; i < methods.length && !flag; ++i) {
			if (methods[i].getName().equals(methodName)) {
				method = methods[i];
				// make the method accessible
				method.setAccessible(true);
				flag = true;
			}
		}
		try {
			o = method.invoke(obj, params);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return o;
	}
	
	private Game newGame(List<Player> playerList){
		String tournamentName = "Glorio's Open";
		List<String> listCardNames = new ArrayList<String>();
		listCardNames.add("T3");
		listCardNames.add("G4");
		hole = new Hole(listCardNames);
		List<Hole> listHole = new ArrayList<Hole>();
		listHole.add(hole);
		Tournament tour = new Tournament(tournamentName, playerList, listHole);
		return new Game(tour);
	}
	
	private List<Player> create4Players(){
		Player myPlayer1 = new Player("Glorio");
		Player myPlayer2 = new Player("Prosikito");
		Player myPlayer3 = new Player("Ambrosio");
		Player myPlayer4 = new Player("Glorio");
		List<Player> playerList = new ArrayList<Player>();
		myPlayer1.setBallPosition(new Point(0, 0));
		myPlayer2.setBallPosition(new Point(0, 2));
		myPlayer3.setBallPosition(new Point(0, 4));
		myPlayer4.setBallPosition(new Point(0, 6));
		playerList.add(myPlayer1);
		playerList.add(myPlayer2);
		playerList.add(myPlayer3);
		playerList.add(myPlayer4);
		return playerList;
	}
	
	private List<Player> create4PlayersNextToHole(List<Point> nextToHolePoints){
		Player myPlayer1 = new Player("Glorio");
		Player myPlayer2 = new Player("Prosikito");
		Player myPlayer3 = new Player("Ambrosio");
		Player myPlayer4 = new Player("Glorio");
		List<Player> playerList = new ArrayList<Player>();
		myPlayer1.setBallPosition(nextToHolePoints.get(0));
		myPlayer2.setBallPosition(nextToHolePoints.get(1));
		myPlayer3.setBallPosition(nextToHolePoints.get(2));
		myPlayer4.setBallPosition(nextToHolePoints.get(3));
		playerList.add(myPlayer1);
		playerList.add(myPlayer2);
		playerList.add(myPlayer3);
		playerList.add(myPlayer4);
		return playerList;
	}
}
