package models;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

public class TestTournament {
	
	/**
	 * Attribute Tournament t.
	 */
	static Tournament t;
	/**
	 * Attribute List<Player> players.
	 */
	static List<Player> players = new ArrayList<Player>();
	/**
	 * Attribute List<Player> players2.
	 */
	static List<Player> players2 = new ArrayList<Player>();
	/**
	 * Attribute List<Hole> holes.
	 */
	static List<Hole> holes = new ArrayList<Hole>();
	/**
	 * Attribute List<Hole> holes2.
	 */
	static List<Hole> holes2 = new ArrayList<Hole>();
	
	/**
	 * @fn setUp()
	 * @throws Exception
	 */
	@Test
	public void setUp() throws Exception {
		
		
		players.add(new Player("Pepe"));
		players.add(new Player("Messi"));
		
		players2.add(new Player("Cristiano"));
		players2.add(new Player("Guti"));
		
		
		holes.add(new Hole(2));
		holes.add(new Hole(1));
		holes.add(new Hole(3));
		
		holes2.add(new Hole(1));
		holes2.add(new Hole(3));
		holes2.add(new Hole(2));
		
		t=new Tournament("Torneo Archienemigos", players, holes );
	}
	
	/**
	 * @fn testTournament()
	 */
	@Test
	public void testTournament() {
		Assert.assertTrue("Incorrect turnament name", "Torneo Archienemigos".equals(t.getTournamentName()));
		Assert.assertTrue("Incorrect players list", players.equals(t.getPlayers()));
		Assert.assertTrue("Incorrect holes list", holes.equals(t.getHoles()));
	}
	
	/**
	 * @fn testGetTournamentName()
	 */
	@Test
	public void testGetTournamentName(){
		Assert.assertTrue("Error in getTournamentName()", "Torneo Archienemigos".equals(t.getTournamentName()));
	}
	
	/**
	 * @fn testSetTournamentName()
	 */
	@Test
	public void testSetTournamentName(){
		t.setTournamentName("Torneo Mejores Amigos");
		Assert.assertTrue("Error in setTournamentName(String)", "Torneo Mejores Amigos".equals(t.getTournamentName()));
	}
	
	/**
	 * @fn testGetHoles()
	 */
	@Test
	public void testGetHoles(){
		Assert.assertTrue("Error in getHoles()", holes.equals(t.getHoles()));
	}
	
	/**
	 * @fn testSetHoles()
	 */
	@Test
	public void testSetHoles(){
		t.setHoles(holes2);
		Assert.assertTrue("Error in setHoles(List<Hole>)", holes2.equals(t.getHoles()));
	}
	
	/**
	 * @fn testGetPlayers()
	 */
	@Test
	public void testGetPlayers(){
		Assert.assertTrue("Error in getPlayers()", players.equals(t.getPlayers()));
	}
	
	/**
	 * @fn testSetPlayers()
	 */
	@Test
	public void testSetPlayers(){
		t.setPlayers(players2);
		Assert.assertTrue("Error in setPlayers(List<Player>)", players2.equals(t.getPlayers()));
	}

}
