package models;

import static org.junit.Assert.assertTrue;
import models.Player;
import org.junit.Test;

/**
 * @file TestPlayer
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 4/1/2012
 * 
 * Class with Junit test of Player class.
 */
public class TestPlayer {
	
	/**
	 * Attribute player p.
	 */
	static Player p;	

	/**
	 * @fn setUpBeforeClass()
	 * @throws Exception
	 */
	@Test
	public void setUpBeforeClass() throws Exception {
		p = new Player("Paco");
	}

	/**
	 * @fn tesardownAfterClass()
	 * @throws Exception
	 */
	@Test
	public void tearDownAfterClass() throws Exception {
	}

	/**
	 * @fn setUp()
	 * @throws Exception
	 */
	@Test
	public void setUp() throws Exception {
		p = new Player("Paco");
	}

	/**
	 * @fn tearDown()
	 * @throws Exception
	 */
	@Test
	public void tearDown() throws Exception {
	}

	/**
	 * @fn testPlayerName()
	 */
	@Test
	public void testPlayerName() {
		assertTrue("Incorrect player name", "Paco".equals(p.getPlayerName()));
	}
	
	/**
	 * @fn testPlayerName2()
	 */
	@Test
	public void testPlayerName2() {
		p.setPlayerName("Norberto");
		assertTrue("Incorrect player name", "Norberto".equals(p.getPlayerName()));
	}
	
	/**
	 * @fn testCreateMap()
	 */
	@Test
	public void testCreateMap() {
		assertTrue("Incorrect skill token in club", p.getSkillTokenInClub("Putter")==0);
	}
	
	/**
	 * @fn testMapSkillToken()
	 */
	@Test
	public void testMapSkillToken() {
		p.addSkillTokenInClub("Driver", 2);
		assertTrue("Incorrect skill token in club", p.getSkillTokenInClub("Driver")==2);
	}
	
	/**
	 * @fn testAddSkillToken()
	 */
	@Test
	public void testAddSkillToken() {
		p.addSkillTokenInClub("Driver");		
		assertTrue("Incorrect skill token in club", p.getSkillTokenInClub("Driver")==3);
	}
	
	/**
	 * @fn testRemoveSkillToken()
	 */
	@Test
	public void testRemoveSkillToken() {
		p.addSkillTokenInClub("3Iron", 4);
		p.removeTokenInClub("3Iron");
		assertTrue("Incorrect skill token in club", p.getSkillTokenInClub("3Iron")==3);
	}
	
	/**
	 * @fn testRemoveSkillToken2()
	 */
	@Test
	public void testRemoveSkillToken2() {
		p.addSkillTokenInClub("7Iron",5);
		p.removeToken("7Iron",4);
		assertTrue("Incorrect skill token in club", p.getSkillTokenInClub("7Iron")==1);
	}
}
