package models;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * @file TestDice
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 4/1/2012
 * 
 * Class with Junit test of Dice class.
 */
public class TestDice{

	/**
	 * @fn testRoll()
	 * 
	 * Check if the value of the dice is correct.
	 */
	@Test
	public void testRoll(){
		Integer a=Dice.roll();
		assertTrue("El valor al tirar un dado no es correcto", a>0);
		assertTrue("El valor al tirar un dado no es correcto", a<=6);
	}
}
