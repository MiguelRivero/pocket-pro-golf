package models;

import static org.junit.Assert.*;
import java.awt.Point;
import java.util.*;
import mains.Throw;
import org.junit.*;

import utilities.Clubs;

/**
 * @file TestMovement
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 4/1/2012
 * 
 * Class with Junit test of Movement class.
 */
public class TestMovement {
	
	private static Hole holemock;
	
	/**
	 * @fn setUpBeforeClass()
	 * @throws java.lang.Exception
	 */
	@Test
	public void setUpBeforeClass() throws Exception {
		List<String> cardNames= new ArrayList<String>();
		cardNames.add("T1");
		cardNames.add("F1");
		cardNames.add("F10");
		cardNames.add("G4");
		holemock= new HoleMock(cardNames);		
	}

	/**
	 * @fn tearDownAfterClass()
	 * @throws java.lang.Exception
	 */
	public void tearDownAfterClass() throws Exception {
	}

	/**
	 * @fn setUp()
	 * @throws java.lang.Exception
	 */
	public void setUp() throws Exception {
	}

	/**
	 * @fn tearDown()
	 * @throws java.lang.Exception
	 */
	public void tearDown() throws Exception {
	}

	/**
	 * @fn testMovement()
	 * 
	 * Test method for {@link models.Movement#Movement(models.Hole)}.
	 */
	@Test
	public void testMovement() {
		//Hole holeMockito= Mockito.mock(Hole.class);
		//Mockito.when(holeMockito.getWind()).thenReturn(new Point(1, 0));
		//Mockito.doReturn(4).when(holeMockito).getWind();
		Movement m= new Movement(holemock);
		assertTrue("Movement hole wrong build", m.getHole().equals(holemock));
		assertTrue("Movement wind wrong build", m.getWind().equals(new Point(1, 0)));
		assertTrue("Movement direction wrong build", m.getDirection().equals(new Point(0, 1)));
	}

	/**
	 * @fn testCalculateGroundAndSandMovement()
	 * 
	 * Test method for {@link models.Movement#calculateGroundAndSandMovement(java.awt.Point, java.lang.Integer)}.
	 */
	@Test
	public void testCalculateGroundAndSandMovement() {
		Movement m= new Movement(holemock);
		Point actualPosition = new Point(3,4);
		Integer progress= 2;
		Point result = m.calculateGroundAndSandMovement(actualPosition, progress);
		Point expected= new Point(3,6);
		assertTrue("Error 1", result.equals(expected));
	}

	/**
	 * @fn testCalculateGroundMovement1()
	 * 
	 * Test method for {@link models.Movement#calculateGroundMovement(java.awt.Point, java.lang.String, mains.Throw)}.
	 */
	@Test
	public void testCalculateGroundMovement1() {
		Throw th=new Throw();
		th.fastThrow();
		Movement m= new Movement(holemock);
//		m.getWind();
		
//		System.out.println(m.getHole());
//		System.out.println(m.getHole().getStartPoint());
//		System.out.println(th.getProgress());
//		System.out.println(th.getRollDoubles());
//		System.out.println(th.getRollTriples());
		Point returned = m.calculateGroundMovement(new Point (3,7), "3Iron", th);
		Point expected= new Point();
		if(th.getRollDoubles()  )
			expected = new Point(3+1, 7+th.getProgress());
		else if(th.getRollTriples())
			expected = new Point(3-1, 7+th.getProgress());
		else
			expected = new Point(3, 7+th.getProgress());
		 assertTrue("Error de cojones", returned.equals(expected));

	}
	
	/**
	 * @fn testCalculateGroundMovement1()
	 * 
	 * Test method for {@link models.Movement#calculateGroundMovement(java.awt.Point, java.lang.String, mains.Throw)}.
	 */
	//comprobando wedge desde arena
	@Test
	public void testCalculateGroundMovement2() {
		Throw th=new Throw();
		th.fastThrow();
		Movement m= new Movement(holemock);
		
//		System.out.println(m.getHole());
//		System.out.println(m.getHole().getStartPoint());
//		System.out.println(th.getProgress());
//		System.out.println(th.getRollDoubles());
//		System.out.println(th.getRollTriples());
		Point returned = m.calculateGroundMovement(new Point (2,12), Clubs.getWedge(), th);
		Point expected= new Point();
		if(th.getRollDoubles() )
			expected = new Point(2+1, 12+th.getProgress()-1);
		else if(th.getRollTriples())
			expected = new Point(2-1, 12+th.getProgress()-1);
		else
			expected = new Point(2, 12+th.getProgress()-1);
//		System.out.println("Devuelto = (" + returned.x+","+returned.y+")");
//		System.out.println("Calculado= (" + expected.x+","+expected.y+")");
		 assertTrue("Error de cojones2", returned.equals(expected));

	}
	
	/**
	 * @fn testCalculateGroundMovement3()
	 * 
	 * Test method for {@link models.Movement#calculateGroundMovement(java.awt.Point, java.lang.String, mains.Throw)}.
	 */
	//comprobando wedge desde fariway
	@Test
	public void testCalculateGroundMovement3() {
		Throw th=new Throw();
		th.fastThrow();
		Movement m= new Movement(holemock);
		
//		System.out.println(m.getHole());
//		System.out.println(m.getHole().getStartPoint());
//		System.out.println(th.getProgress());
//		System.out.println(th.getRollDoubles());
//		System.out.println(th.getRollTriples());
		Point returned = m.calculateGroundMovement(new Point (2,10), Clubs.getWedge(), th);
		Point expected= new Point();
//		if(th.getRollDoubles() )
//			expected = new Point(2+1, 12+th.getProgress()-1);
//		else if(th.getRollTriples())
//			expected = new Point(2-1, 12+th.getProgress()-1);
//		else
			expected = new Point(2, 11);
//		System.out.println("Devuelto = (" + returned.x+","+returned.y+")");
//		System.out.println("Calculado= (" + expected.x+","+expected.y+")");
		 assertTrue("Error de cojones3", returned.equals(expected));

	}
	
	/**
	 * @fn testCalculateGroundMovement4()
	 * 
	 * Test method for {@link models.Movement#calculateGroundMovement(java.awt.Point, java.lang.String, mains.Throw)}.
	 */
	//comprobando wedge desde rough
	@Test
	public void testCalculateGroundMovement4() {
		Throw th=new Throw();
		th.fastThrow();
		Movement m= new Movement(holemock);
		
//		System.out.println(m.getHole());
//		System.out.println(m.getHole().getStartPoint());
//		System.out.println(th.getProgress());
//		System.out.println(th.getRollDoubles());
//		System.out.println(th.getRollTriples());
		Point returned = m.calculateGroundMovement(new Point (3,28), Clubs.getWedge(), th);
		Point expected= new Point();
		if(th.getRollDoubles())
			expected = new Point(3+1, 28+th.getProgress()-1);
		else if(th.getRollTriples())
			expected = new Point(3-1, 28+th.getProgress()-1);
		else
			expected = new Point(3, 28+th.getProgress()-1);
		
//		System.out.println("Devuelto = (" + returned.x+","+returned.y+")");
//		System.out.println("Calculado= (" + expected.x+","+expected.y+")");
		assertTrue("Error throwning with a Wedge in rought", returned.equals(expected));
			
	}
	 
	
	/**
	 * @fn testCalculateAirMovement()
	 * 
	 * Test method for {@link models.Movement#calculateAirMovement(java.awt.Point, java.lang.String, mains.Throw)}.
	 */
	@Test
	public void testCalculateAirMovement() {
		Throw th=new Throw();
		th.fastThrow();
		Movement m= new Movement(holemock);
		
//		System.out.println(m.getHole());
//		System.out.println(m.getHole().getStartPoint());
//		System.out.println(th.getProgress());
//		System.out.println(th.getRollDoubles());
//		System.out.println(th.getRollTriples());
		Point returned = m.calculateGroundMovement(new Point (0,19), Clubs.getWedge(), th);
		Point expected= new Point();
		expected = new Point(0, 20);
//		System.out.println("Devuelto = (" + returned.x+","+returned.y+")");
//		System.out.println("Calculado= (" + expected.x+","+expected.y+")");
		assertTrue("Error throwning with a Wedge in rought", returned.equals(expected));
		

	}

	/**
	 * @fn testCalculateFirstAndSecondAirMovement()
	 * 
	 * Test method for {@link models.Movement#calculateFirstAndSecondAirMovement(java.awt.Point, java.awt.Point, java.lang.String, mains.Throw)}.
	 */
	@Test
	public void testCalculateFirstAndSecondAirMovement() {
		Throw th=new Throw();
		th.fastThrow();
		Movement m= new Movement(holemock);
		Point aim= new Point(0, 1);
	
		Point returned = m.calculateFirstAndSecondAirMovement(new Point (3,0), aim, Clubs.getIron7(), th);
		Point expected= new Point();
		if(th.getRollDoubles())
			expected = new Point(3+1, 0+th.getProgress()+2);
		else if(th.getRollTriples())
			expected = new Point(3-1, 0+th.getProgress()+2);
		else
			expected = new Point(3, 0+th.getProgress()+2);
		
		assertTrue("Error throwning with a Wedge in rought", returned.equals(expected));
	}

	/**
	 * @fn testGetValidPositionOfOutOfBoundPenalty()
	 * 
	 * Test method for {@link models.Movement#getValidPositionOfOutOfBoundPenalty(java.awt.Point)}.
	 */
	@Test
	public void testGetValidPositionOfOutOfBoundPenalty() {
		Throw th=new Throw();
		th.fastThrow();
		Movement m= new Movement(holemock);
		Point actualPosition= new Point(-1,5);
		Point returned = m.getValidPositionOfOutOfBoundsPenalty(actualPosition);
		Point expected= new Point(0, 5);

		assertTrue("Error out of bound position left", returned.equals(expected));
		
	}

	/**
	 * @fn testGetValidPositionOfWaterPenalty()
	 * 
	 * Test method for {@link models.Movement#getValidPositionOfWaterPenalty(java.awt.Point)}.
	 */
	public void testGetValidPositionOfWaterPenalty() {
		Throw th=new Throw();
		th.fastThrow();
		Movement m= new Movement(holemock);
		Point actualPosition= new Point(1,29);
	
		Point returned = m.getValidPositionOfWaterPenalty(actualPosition);
		Point expected= new Point(2, 29);

		assertTrue("Error get the right tile when in water", returned.equals(expected));
	}

	/**
	 * @fn testSetDirection()
	 * Test method for {@link models.Movement#setDirection(java.awt.Point)}.
	 */
	@Test
	public void testSetDirection() {
		/*Hole holeMockito= Mockito.mock(Hole.class);
		Mockito.doReturn(4).when(holeMockito).getWind();
		*/
		Movement m= new Movement(holemock);
		m.setDirection(new Point(3,5));
		assertTrue("Movement hole wrong build", m.getDirection().equals(new Point(3,5)));
	}
	
	/**
	 * @fn testIsPastTheHole()
	 * Test method for {@link models.Movement#getDoublesOrTriples(mains.Throw)}.
	 */
	@Test
	public void testgetDoublesOrTriples() {
		Throw th=new Throw();
		th.fastThrow();
		Movement m= new Movement(holemock);
				
		Point returned = m.getDoublesOrTriples(th);
		Point expected= new Point(0,0);
			
		
		if(th.getRollDoubles()){
			expected= new Point(1,0);
			assertTrue("Error out of bound position left", returned.equals(expected));
		}else if (th.getRollTriples()){
			expected= new Point(1,0);
			assertTrue("Error out of bound position left", returned.equals(expected));
		}else{
			assertTrue("Error out of bound position left", returned.equals(expected));
		}
		
	}
	
	/**
	 * @fn testGetValidPositionOfTreePenalty()
	 * Test method for {@link models.Movement#getValidPositionOfTreePenalty(java.awt.Point)}.
	 */
	
	public void testGetValidPositionOfTreePenalty(){
		Throw th=new Throw();
		th.fastThrow();
		Movement m= new Movement(holemock);
		Point actualPosition= new Point(0,19);
		
		Point returned = m.getValidPositionOfTreePenalty(actualPosition);
		Point expected= new Point(1, 17);
		System.out.println("Devuelto = (" + returned.x+","+returned.y+")");
		System.out.println("Calculado= (" + expected.x+","+expected.y+")");
		assertTrue("Error out of bound position left", returned.equals(expected));
	}
}
