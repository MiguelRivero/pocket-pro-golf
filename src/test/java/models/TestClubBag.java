package models;

import org.junit.*;

/**
 * @file TestClubBag
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 4/1/2012
 * 
 * Class with Junit test of ClubBag class.
 */
public class TestClubBag {
	
	/**
	 * Attribute bag.
	 */
	static ClubBag bag;
	
	/**
	 * Attribute bd
	 */
	static Club bd;
	
	/**
	 * @fn setUp()
	 * @throws Exception
	 */
	@Test
	public void setUpBeforeClass() throws Exception {
		bag = new ClubBag();
		bd = bag.getClub("Driver");
	}
	
	/**
	 * @fn testClubBagConstructor()
	 */
	@Test
	public void testClubBagConstructor() {	
		Assert.assertTrue("incorrect ClubBagWedgeCreated", bag.getClubBag().get(0).getClubName().equals("Wedge"));
		Assert.assertTrue("incorrect ClubBagWedgeAttributesCreated", bag.getClubBag().get(0).getAir()==1 && bag.getClubBag().get(0).getGround()==1 && bag.getClubBag().get(0).getTee()==1 && bag.getClubBag().get(0).getRoughAir()==0 && bag.getClubBag().get(0).getRoughGround()==-1);
		Assert.assertTrue("incorrect ClubBagDriverCreated", bag.getClubBag().get(1).getClubName().equals("Driver"));
		Assert.assertTrue("incorrect ClubBagDriverAttributesCreated", bag.getClubBag().get(1).getAir()==3 && bag.getClubBag().get(1).getGround()==4 && bag.getClubBag().get(1).getTee()==1 && bag.getClubBag().get(1).getRoughAir()==2 && bag.getClubBag().get(1).getRoughGround()==0);
		Assert.assertTrue("incorrect ClubBagPutterCreated", bag.getClubBag().get(2).getClubName().equals("Putter"));
		Assert.assertTrue("incorrect ClubBagPutterAttributesCreated", bag.getClubBag().get(2).getAir()==0 && bag.getClubBag().get(2).getGround()==1 && bag.getClubBag().get(2).getTee()==0 && bag.getClubBag().get(2).getRoughAir()==0 && bag.getClubBag().get(2).getRoughGround()==0);
		Assert.assertTrue("incorrect ClubBag3IronCreated", bag.getClubBag().get(3).getClubName().equals("3Iron"));
		Assert.assertTrue("incorrect ClubBag3IronAttributesCreated", bag.getClubBag().get(3).getAir()==3 && bag.getClubBag().get(3).getGround()==3 && bag.getClubBag().get(3).getTee()==1 && bag.getClubBag().get(3).getRoughAir()==1 && bag.getClubBag().get(3).getRoughGround()==0);
		Assert.assertTrue("incorrect ClubBag5IronCreated", bag.getClubBag().get(4).getClubName().equals("5Iron"));
		Assert.assertTrue("incorrect ClubBag5IronAttributesCreated", bag.getClubBag().get(4).getAir()==2 && bag.getClubBag().get(4).getGround()==3 && bag.getClubBag().get(4).getTee()==1 && bag.getClubBag().get(4).getRoughAir()==0 && bag.getClubBag().get(4).getRoughGround()==0);
		Assert.assertTrue("incorrect ClubBag7IronCreated", bag.getClubBag().get(5).getClubName().equals("7Iron"));
		Assert.assertTrue("incorrect ClubBag7IronAttributesCreated", bag.getClubBag().get(5).getAir()==2 && bag.getClubBag().get(5).getGround()==2 && bag.getClubBag().get(5).getTee()==1 && bag.getClubBag().get(5).getRoughAir()==0 && bag.getClubBag().get(5).getRoughGround()==0);
		Assert.assertTrue("incorrect ClubBag9IronCreated", bag.getClubBag().get(6).getClubName().equals("9Iron"));
		Assert.assertTrue("incorrect ClubBag9IronAttributesCreated", bag.getClubBag().get(6).getAir()==2 && bag.getClubBag().get(6).getGround()==1 && bag.getClubBag().get(6).getTee()==1 && bag.getClubBag().get(6).getRoughAir()==1 && bag.getClubBag().get(6).getRoughGround()==0);	
	}
	
	/**
	 * @fn testGetClub()
	 */
	@Test
	public void testGetClub(){
		Club c=new Club("Driver", 3, 4, 1, 2, 0,"pos eso");
		Assert.assertTrue("error getting a club", bd.getClubName().equals(c.getClubName()) && bd.getAir().equals(c.getAir()) && bd.getGround().equals(c.getGround()) && bd.getTee().equals(c.getTee()) && bd.getRoughAir().equals(c.getRoughAir()) && bd.getRoughGround().equals(c.getRoughGround()));
	}
}
