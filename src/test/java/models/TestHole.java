package models;

import java.awt.Point;
import models.Hole;
import org.junit.*;

/**
 * @file TestHole
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 4/1/2012
 * 
 * Class with Junit test of Hole class.
 */
public class TestHole {
	
	/**
	 * Static Attribute hole.
	 */
	static Hole hole;
			
	/**
	 * @fn setUpBeforeClass()
	 * @throws Exception
	 */
	@Test
	public void setUpBeforeClass() throws Exception {		
		hole = new Hole(3);
	}
	
	/**
	 * @fn testConstructorHole()
	 * 
	 * This is the JUnit Test from the constructor of hole and we check out here that these values aren't null.
	 */
	@Test
	 public void testConstructorHole() {		
		Assert.assertTrue("Error Segment", hole.getSegments()!= null);
		Assert.assertTrue("Error Segment", hole.getActualSegment()!= null);
		Assert.assertTrue("Error Segment", hole.getWind()!= null);		
	}
	
	/**
	 * @fn testWind()
	 * 
	 * Checks out points whose values must be equals to 1, -1 or 0
	 */
	@Test
	public void testWind(){		
		Point w = hole.getWind();		
		Assert.assertTrue("Error Point", (w.x==1 || w.x==-1 || w.x==0) && (w.y==1 ||w.y==-1 ||w.y==0));		
	}
	
	/**
	 * @fn testJoinCards()
	 */
	@Test
	public void testJoinCards(){		
	}
}
