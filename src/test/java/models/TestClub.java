package models;

import org.junit.Test;
import junit.framework.Assert;
import models.Club;

/**
 * @file TestClub
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 4/1/2012
 * 
 * Class with Junit test of Club class.
 */
public class TestClub {
	
	/**
	 * Attribute club.
	 */
	static Club c;
	
	/**
	 * @fn setUp()
	 * @throws Exception
	 */
	@Test
	public void setUp() throws Exception {
		c=new Club("7Iron",3, 4, 1, 0, 0, "palo molon");
	}
	
	/**
	 * @fn testClub()
	 */
	@Test
	public void testClub() {
		Assert.assertTrue("incorrect club name", "7Iron".equals(c.getClubName()));
		Assert.assertTrue("incorrect air property content", c.getAir()==3);
		Assert.assertTrue("incorrect ground property content", c.getGround()==4);
		Assert.assertTrue("incorrect tee property content", c.getTee()==1);
		Assert.assertTrue("incorrect roughAir property content", c.getRoughAir()==0);
		Assert.assertTrue("incorrect roughGround property content", c.getRoughGround()==0);
		Assert.assertTrue("incorrect description property content", "palo molon".equals(c.getDescription()));
	}
	
	/**
	 * @fn testGetName()
	 */
	@Test
	public void testGetName(){
		Assert.assertTrue("error getting name", "7Iron".equals(c.getClubName()));
	}
	
	/**
	 * @fn testGetAir()
	 * 
	 * 
	 */
	@Test
	public void testGetAir(){
		Assert.assertTrue("error gettingAir", c.getAir()==3);
	}
	
	/**
	 * @fn testGetGround()
	 */
	@Test
	public void testGetGround(){
		Assert.assertTrue("error gettingGround", c.getGround()==4);
	}
	
	/**
	 * @fn testGetTee()
	 */
	@Test
	public void testGetTee(){
		Assert.assertTrue("error gettingGround", c.getTee()==1);
	}
	
	/**
	 * @fn testGetRoughAir()
	 */
	@Test
	public void testGetRoughAir(){
		Assert.assertTrue("error gettingRoughtAir", c.getRoughAir()==0);
	}
	
	/**
	 * @fn testGetRoughGround()
	 */
	@Test
	public void testGetRoughGround(){
		Assert.assertTrue("error gettingRoughtGround", c.getRoughGround()==0);
	}	
}
