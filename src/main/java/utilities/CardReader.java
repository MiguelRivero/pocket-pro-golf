package utilities;

import java.io.IOException;
import java.util.*;

/**
 * @file CardReader.
 * @brief Abstract class.
 * @author Grupo 14, ISG2.
 * @date 30/10/2011
 * @version 0.1
 * 
 * Abstract class with utility methods to read cards in .txt files, and load any card. 
 * Reading can be random or specific.
 */
public abstract class CardReader{
	
	/**
	 * @fn List<Object> readCard(String file).
	 * @brief Paints the card about the screen, with its name.
	 * @param String file: txt file with cards.
	 * @return List<Object> with it's name, and HoleCard.
	 * @throws IOException
	 */
	public static List<Object> readCard(String file) throws IOException {
		InputStream fe = new InputStream(file);
		ArrayList<String> lines = new ArrayList<String>();

		for (String line : fe) {
			lines.add(line);
		}
		
		//Se busca carta (l�nea) aleatoria
		String card = lines.get(new Random().nextInt(lines.size()));

		ArrayList<String> v = Strings.separateElements(card, ":");
		String name = v.get(0);

		List<Object> listObject = new ArrayList<Object>();
		listObject.add(name);
		listObject.add(loadCard(v.get(1)));

		return listObject;
	}
	
    /**
	 * @fn List<Object> readCard(String file, String cardName).
	 * @brief Looking for card specific in a file.
	 * @param String file: file where to look.
	 * @param String cardName: card we wish to find.
	 * @return List<Object> with its name, and HoleCard.
	 * @throws IOException
	 */
	public static List<Object> readCard(String file, String cardName) throws IOException {
		// Cargar fichero
		InputStream fe = new InputStream(file);
		ArrayList<String> lines = new ArrayList<String>();

		for (String line : fe) {
			lines.add(line);
		}

		// Se busca carta (l�nea)
		List<Object> listObject = new ArrayList<Object>();

		for (String line : lines) {
			ArrayList<String> v = Strings.separateElements(line, ":");
			String name = v.get(0);

			// Carga de la carta si coincide con el nombre
			if (name.compareToIgnoreCase(cardName) == 0) {
				List<List<String>> holeCard = loadCard(v.get(1));
				listObject.add(name);
				listObject.add(holeCard);
			}
		}

		if (listObject.size() == 0) {
			throw new IllegalArgumentException("No existe la carta " + cardName);
		}

		return listObject;
	}
	
    /**
	 * @fn List<List<String>> loadCard (String card).
	 * @brief Builds the card that its name has been passed as a parameter.
	 * @param String card: card that we want build.
	 * @return List<List<String>> that make up a card.
	 */
	private static List<List<String>> loadCard(String card) {
		ArrayList<List<String>> holeSegment = new ArrayList<List<String>>();
		ArrayList<String> grounds = Strings.separateElements(card, ",");

		Integer numOfElemAdded = 0;
		List<String> list = new ArrayList<String>();

		for (String surface : grounds) {
			list.add(surface);
			numOfElemAdded++;

			if (numOfElemAdded % 7 == 0) {
				holeSegment.add(list);
				list = new ArrayList<String>();
			}
		}

		Collections.reverse(holeSegment);

		return holeSegment;
	}
}
