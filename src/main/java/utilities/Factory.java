package utilities;

import java.io.*;
import java.util.List;
import cards.*;
import mains.*;
import models.*;

/**
 * @file Factory.
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 25/11/2011
 * 
 * Factory class with static methods useful in the game.
 */
public abstract class Factory{
	
	/**
	 * @fn HoleCard newFairway().
	 * @brief Constructor of card Fairway.
	 * @return HoleCard Fairway.
	 * @throws IOException.
	 */
	public static HoleCard newFairway() throws IOException{
		return new Fairway();
	}
	
	/**
	 * @fn HoleCard newFairway (String card).
	 * @brief Constructor of card Fairway with params.
	 * @param String card.
	 * @return HoleCard Fairway.
	 * @throws IOException.
	 */
	public static HoleCard newFairway(String card) throws IOException{
		return new Fairway(card);
	}
	
	/**
	 * @fn HoleCard newGreen().
	 * @brief Constructor of card Green.
	 * @return HoleCard Green.
	 * @throws IOException.
	 */
	public static HoleCard newGreen() throws IOException{
		return new Green();
	}
	
	/**
	 * @fn HoleCard newGreen(String card).
	 * @brief Constructor of card Green with params.
	 * @param String card.
	 * @return HoleCard Green.
	 * @throws IOException.
	 */
	
	public static HoleCard newGreen(String card) throws IOException{
		return new Green(card);
	}
	
	/**
	 * @fn HoleCard newHoleCard(String file).
	 * @brief Constructor of HoleCard with params.
	 * @param String file: File where is the cards.
	 * @return new HoleCard.
	 * @throws IOException.
	 */
	public static HoleCard newHoleCard(String file) throws IOException{
		return new HoleCard(file);
	}
	
	/**
	 * @fn HoleCard newHoleCard(String file, String card).
	 * @brief Constructor of HoleCard.
	 * @param String file: File where is the card.
	 * @param String card: Card desired.
	 * @return new HoleCard.
	 * @throws IOException
	 */
	public static HoleCard newHoleCard(String file, String card) throws IOException{
		return new HoleCard(file,card);
	}
	
	/**
	 * @fn HoleCard newTee().
	 * @brief Construcor of card Tee.
	 * @return HoleCard Tee.
	 * @throws IOException.
	 */
	public static HoleCard newTee() throws IOException{
		return new Tee();
	}
	
	/**
	 * @fn HoleCard newTee(String card).
	 * @brief Construcor of card Tee with params.
	 * @param String card: Card specific card that the game needs.
	 * @return HoleCard Tee.
	 * @throws IOException.
	 */
	public static HoleCard newTee(String card) throws IOException{
		return new Tee(card);
	}
	
	/**
	 * @fn Game newGame(Tournament tour). 
	 * @brief Constructor of the class with params.
	 * @param Tournament tour.
	 * @return Game.
	 */
	public static Game newGame(Tournament tour){
		return new Game(tour);
	}
	
	/**
	 * @fn Throw newThrow().
	 * @brief Constructor of Throw.
	 * @return Throw.
	 */
	public static Throw newThrow(){
		return new Throw();
	}
	
	/**
	 * @fn Club newClub().
	 * @brief Default constructor of the class.
	 * @return Club.
	 */
	public static Club newClub(){
		return new Club();
	}
	
	/**
	 * @fn Club newClub(String name, Integer a, Integer g, Integer t, Integer rAir ,Integer rGround,String desc).
	 * @brief Constructor with all attributes of the class.
	 * @param String name.
	 * @param Integer a.
	 * @param Integer g.
	 * @param Integer t.
	 * @param Integer rAir.
	 * @param Integer rGround.
	 * @param String desc.
	 * @return Club.
	 */	
	public static Club newClub(String name, Integer a, Integer g, Integer t, Integer rAir, Integer rGround, String desc){
		return new Club(name, a, g, t, rAir, rGround, desc);
	}
	
	/**
	 * @fn ClubBag newClubBag().
	 * @brief Constructor of ClubBag.
	 * @return ClubBag.
	 */
	public static ClubBag newClubBag(){
		return new ClubBag();
	}	
	
	/**
	 * @fn Hole newHole(Integer numberOfFairways).
	 * @brief Constructor of Hole with params.
	 * @param Integer numberOfFairways: Number of cards desired in the hole.
	 * @return Hole.
	 * @throws IOException.
	 */
	public static Hole newHole(Integer numberOfFairways) throws IOException{
		return new Hole(numberOfFairways);
	}
	
	/**
	 * @fn Hole newHole(List<String> cardNames).
	 * @brief Constructor of Hole with params.
	 * @param List<String> cardNames: a list with the hole cards that compose the hole.
	 * @return Hole.
	 * @throws IOException.
	 */
	public static Hole newHole(List<String> cardNames) throws IOException{
		return new Hole(cardNames);
	}
	
	/**
	 * @fn HoleMock newHoleMock(Integer numberOfFairways).
	 * @brief Constructor of the class with params.
	 * @param Integer numberOfFairways.
	 * @return HoleMock.
	 * @throws IOException
	 */
	public static HoleMock newHoleMock(Integer numberOfFairways) throws IOException{
		return new HoleMock(numberOfFairways);
	}

	/**
	 * @fn HoleMockHoleMock(List<String> cardNames).
	 * @brief Constructor of the class with params.
	 * @param List<String> cardNames.
	 * @return HoleMock
	 */
	public static HoleMock newHoleMock(List<String> cardNames){
		return new HoleMock(cardNames);
	}
	
	/**
	 * @fn Movement newMovement(Hole h).
	 * @brief Constructor of Movement.
	 * @param Hole h.
	 * @return Movement.
	 */
	public static Movement newMovement(Hole h){
		return new Movement(h);
	}
	
	/**
	 * @fn Player newPlayer(String name).
	 * @brief Constructor of Player with params.
	 * @param String name.
	 * @return Player.
	 */
	public static Player newPlayer(String name){
		return new Player(name);
	}
	
	/**
	 * @fn TournamentTournament(String n, List<Player> p, List<Hole> h).
	 * @brief Constructor of Tournament.
	 * @param n name of the tournament.
	 * @param p list of players.
	 * @param h list of holes.
	 * @return Tournament.
	 */	
	public static Tournament newTournament(String n, List<Player> p, List<Hole> h){
		return new Tournament(n, p, h);
	}
}
