package utilities;

import java.io.IOException;
import java.util.*;

import models.Hole;

/**
 * @file TournamentReader.
 * @brief Abstract class.
 * @author Grupo 14, ISG2.
 * @date 30/10/2011
 * @version 0.1
 * 
 * Abstract class with utility methods to read tournaments in .txt files, and load any tournament.
 */
public abstract class TournamentReader{
	
	/**
	 * @fn List<String> readTournamentNames(String file).
	 * @brief Return a list with all the names of tournaments.
	 * @param String file: txt file with cards.
	 * @return List<String>.
	 * @throws IOException
	 */
	public static List<String> readTournamentNames(String file) throws IOException {
		InputStream fe = new InputStream(file);
		ArrayList<String> lines = new ArrayList<String>();

		for (String line : fe) {
			lines.add(line);
		}
		
		List<String> names = new ArrayList<String>();
		for (String name : lines){
			names.add(Strings.separateDelimiterElements(name, ":").get(0));
		}

		return names;
	}
	
	/**
	 * @fn List<Hole> readTournament(String file, String nameTournament).
	 * @brief Return a list with the holes of the tournament pased for params.
	 * @param String file: txt file with cards.
	 * @param String nameTournament: name of the tournament to search.
	 * @return List<Hole>.
	 * @throws IOException
	 */
	public static List<Hole> readTournament(String file, String nameTournament) throws IOException {
		InputStream fe = new InputStream(file);
		ArrayList<String> lines = new ArrayList<String>();

		for (String line : fe) {
			lines.add(line);
		}
		
		List<Hole> listHoles = new ArrayList<Hole>();
		
		// Se busca el torneo (l�nea)
		for (String line : lines) {
			ArrayList<String> v = Strings.separateElements(line,":");
			String name = v.get(0);

			//Carga del torneo si coincide con el nombre
			if (name.compareToIgnoreCase(nameTournament) == 0) {
				String tournament = v.get(1);
				List<String> allHoles = Strings.separateElements(tournament,";");	
				
				for (String h : allHoles){
					listHoles.add( Factory.newHole(Strings.separateElements(h,",")) );
				}
			}
		}

		if (listHoles.size() == 0) {
			throw new IllegalArgumentException("No existe el torneo " + nameTournament);
		}

		return listHoles;
	}
}
