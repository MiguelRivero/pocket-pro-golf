package utilities;

/**
 * @brief ChangeCardException
 * @author Grupo 14, ISG2
 * @date 1/11/2011
 * @version 0.1
 */
@SuppressWarnings("serial")
public class ChangeCardException extends RuntimeException{	
	
    /**
	 * @fn ChangeCardException().
	 * @brief Exception change the card. Calls the constructor of RuntimeException.
	 */
	public ChangeCardException(){
		super();
	}
	
    /**
	 * @fn ChangeCardException(String message).
	 * @brief Exception change the card.
	 * @param String message.
	 */
	public ChangeCardException(String message){
		super(message);
	}
}
