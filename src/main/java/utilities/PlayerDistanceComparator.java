package utilities;

import java.awt.Point;
import java.util.Comparator;
import models.Player;

/**
 * @file PlayerDistanceComparator.
 * @date 4/11/2011
 * @author Grupo 14, ISG2.
 * @version 0.1
 *
 * Class with method Constructor that will used in class PlayerOrderComparator.
 */
public class PlayerDistanceComparator implements Comparator<Point>{
	
	/**
	 * Player Atributte.
	 */
	private final Player player;
	
    /**
     * @fn PlayerDistanceComparator(Player player).
     * @brief Constructor to initialize player.
     * @param player.
     */
	public PlayerDistanceComparator(Player p){
		this.player = p;
	}
	
	/**
	 * @fn compare(Point a, Point b).
	 * @brief Compare two points and returns the distance between them.
	 * @param Point a.
	 * @param Point b.
	 * @return int.
	 */
	public int compare(Point a, Point b){
		int result;
		Double distanceToA = player.getBallPosition().distance(a);
		Double distanceToB = player.getBallPosition().distance(b);
		result= distanceToA.compareTo(distanceToB);
		return result;
	}
}
