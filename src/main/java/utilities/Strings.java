package utilities;

import java.util.*;

/**
 * @file Strings.
 * @author Grupo 14, ISG2.
 * @date 11/01/2012
 * @version 0.1
 * 
 * Class with methods to separate element Vector�s.
 */
public class Strings{
	
	/**
	 * @fn ArrayList<String> separateElements (String string, String delimiter).
	 * @brief Separate Elements.
     * @param String string: String with which we will separate our new Vector.
     * @param String delimiter: Delimiter with which we will separate.
     * @return ArrayList<String>.
     * 
     * Separate the String "string" into substrings that are stored in a vector.
     * The "delimiter" parameter is a string whose characters will be used as separators. 
     */
	public static ArrayList<String> separateElements(String string, String delimiter){
		ArrayList<String> list=new ArrayList<String>();
        StringTokenizer stringtokenizer=new StringTokenizer(string,delimiter);
        
        while(stringtokenizer.hasMoreTokens()){
        	list.add(stringtokenizer.nextToken());
        }   
        return list;
	}
	
	/**
	  * @fn ArrayList<String> separateDelimiterElements (String string, String delimiter).
	  * @brief Separate Elements.
	  * @param String string: String with which we will separate our new Vector.
      * @param String delimiter: Delimiter with which we will separate.
      * @return ArrayList<String>.
      * 
      * Separate the String "string" into substrings that are stored in a vector.
      * The "delimiter" parameter is a string whose characters will be used as separators. 
      */	
	public static ArrayList<String> separateDelimiterElements(String string, String delimiter){
		ArrayList<String> vector=new ArrayList<String>();
        StringTokenizer stringtokenizer=new StringTokenizer(string,delimiter,true);
        
        while(stringtokenizer.hasMoreTokens()){
            vector.add(stringtokenizer.nextToken());
        }
        return vector;
	}
}
