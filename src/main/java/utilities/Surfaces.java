package utilities;

/**
 * @file Surfaces.
 * @author Grupo 14, ISG2.
 * @version 0.1.
 * @date 11/01/2012.
 * 
 * Class that holds the different types of surfaces found in the game. Each type is stored in a String.
 */
public class Surfaces{
	
	/**
	 * Rough type static attribute. 
	 */
	private static final String ROUGH = "R";
	
	/**
	 * Fairway type static attribute. 
	 */
	private static final String FAIRWAY = "F";
	
	/**
	 * Tee type static attribute. 
	 */
	private static final String TEE = "T";
	
	/**
	 * Sand type static attribute. 
	 */
	private static final String SAND = "S";
	
	/**
	 * Green type static attribute. 
	 */
	private static final String GREEN = "G";
	
	/**
	 * Next to hole type static attribute. 
	 */
	private static final String NEXT_TO_HOLE = "N";

	/**
	 * Tree type static attribute. 
	 */
	private static final String TREE = "TR";
	
	/**
	 * Water type static attribute. 
	 */
	private static final String WATER = "W";
	
	/**
	 * Rough space where the ball bounces off the tree.
	 */
	private static final String REBOUND = "RT";
	
	/**
	 * @fn String getRough()
     * @brief Getter of the property <tt>ROUGH</tt>.
	 * @return String
	 */
	public static String getRough(){
		return ROUGH;
	}
	
	/**
	 * @fn String getFairway()
     * @brief Getter of the property <tt>FAIRWAY</tt>.
	 * @return String
	 */
	public static String getFairway(){
		return FAIRWAY;
	}

	/**
	 * @fn String getTee()
     * @brief Getter of the property <tt>TEE</tt>.
	 * @return String
	 */
	public static String getTee(){
		return TEE;
	}

	/**
	 * @fn String getSand()
     * @brief Getter of the property <tt>SAND</tt>.
	 * @return String
	 */
	public static String getSand(){
		return SAND;
	}

	/**
	 * @fn String getGreen()
     * @brief Getter of the property <tt>GREEN</tt>.
	 * @return String
	 */
	public static String getGreen(){
		return GREEN;
	}

	/**
	 * @fn String getNextToHole()
     * @brief Getter of the property <tt>NEXT_TO_HOLE</tt>.
	 * @return String
	 */
	public static String getNextToHole(){
		return NEXT_TO_HOLE;
	}

	/**
	 * @fn String getTree()
     * @brief Getter of the property <tt>TREE</tt>.
	 * @return String
	 */
	public static String getTree(){
		return TREE;
	}

	/**
	 * @fn String getWater()
     * @brief Getter of the property <tt>WATER</tt>.
	 * @return String
	 */
	public static String getWater(){
		return WATER;
	}

	/**
	 * @fn String getRebound()
     * @brief Getter of the property <tt>REBOUND</tt>.
	 * @return String
	 */
	public static String getRebound(){
		return REBOUND;
	}
}
