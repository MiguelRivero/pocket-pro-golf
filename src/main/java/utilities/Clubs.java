package utilities;

/**
 * @file Clubs
 * @author Grupo 14, ISG2.
 * @date 2/11/2011
 * @version 0.1
 * 
 * Class where are saved the names of each of the types of Club.
 */
public class Clubs{
	
	/**
	 * Driver type static attribute.
	 */
	private static final String DRIVER = "Driver";
	
	/**
	 * Wedge type static attribute.
	 */
	private static final String WEDGE = "Wedge";
	
	/**
	 * 3Iron type static attribute.
	 */ 
	private static final String IRON3 = "3Iron";
	
	/**
	 * 5Iron type static attribute.
	 */
	private static final String IRON5 = "5Iron";
	
	/**
	 * 7Iron type static attribute.
	 */
	private static final String IRON7 = "7Iron";
	
	/**
	 * 9Iron type static attribute.
	 */
	private static final String IRON9 = "9Iron";
	
    /**
	 * Putter type static attribute.
	 */
	private static final String PUTTER = "Putter";
	
    /**
     * @fn String getDriver().
     * @brief Getter of the property <tt>DRIVER</tt>.
	 * @return String
	 */
	public static String getDriver(){
		return DRIVER;
	}
	
    /**
     * @fn String getWedge()
     * @brief Getter of the property <tt>WEDGE</tt>.
	 * @return String
	 */
	public static String getWedge(){
		return WEDGE;
	}
	
    /**
     * @fn String getIron3()
     * @brief Getter of the property <tt>IRON3</tt>.
	 * @return String
	 */
	public static String getIron3(){
		return IRON3;
	}
	
    /**
	 * @fn String getIron5()
     * @brief Getter of the property <tt>IRON5</tt>.
	 * @return String
	 */
	public static String getIron5(){
		return IRON5;
	}
	
    /**
	 * @fn String getIron7()
     * @brief Getter of the property <tt>IRON7</tt>.
	 * @return String
	 */
	public static String getIron7(){
		return IRON7;
	}
	
    /**
	 * @fn String getIron9()
     * @brief Getter of the property <tt>IRON9</tt>.
	 * @return String
	 */
	public static String getIron9(){
		return IRON9;
	}
	
    /**
	 * @fn String getPutter()
     * @brief Getter of the property <tt>PUTTER</tt>.
	 * @return String
	 */
	public static String getPutter(){
		return PUTTER;
	}	
}