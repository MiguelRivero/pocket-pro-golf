package utilities;

import java.awt.Point;

/**
 * @file ManipulatePoints.
 * @brief Utility class with two methods to manipulate points.
 * @author Grupo 14, ISG2.
 * @version 0.1
 * 
 * Utility class with two methods to manipulate points.
 */
public abstract class ManipulatePoints{
	
	/**
	 * @fn Point sumPoints(Point p1, Point p2).
	 * @brief Sum of two points.
	 * @param Point p1: point number one.
	 * @param Point p2: point number two.
	 * @return Point.
	 */
	public static Point sumPoints(Point p1, Point p2){		
		Point p = new Point(0,0);
		p.setLocation(p1.getX()+p2.getX(), p1.getY()+p2.getY());
		return p;		
	}
	
	/** 
	 * @fn Point reversePoint(Point p1).
	 * @brief Returns the point inverse of the point pased for param.
	 * @param Point p1.
	 * @return Point.
	 */
	public static Point reversePoint(Point p1){		
		Point p = new Point(p1);
		p.setLocation(p.getX()*-1, p.getY()*-1);
		return p;		
	}
}
