package utilities;

/**
 * @file OutOfFieldException.
 * @author Grupo 14, ISG2.
 * @date 4/11/2011
 * @version 0.1
 */
public class OutOfFieldException extends RuntimeException{
	
	/**
	 * Static attribute. 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * @fn OutOfFieldException().
	 * @brief Default constructor.Exception ball out of bounds.Calls the constructor of RuntimeException.
	 */
	public OutOfFieldException(){
		super();
	}
	
	/**
	 * @fn OutOfFieldException(String message).
	 * @brief Exception ball out of bounds.
	 * @param String message.
	 */
	public OutOfFieldException(String message){
		super(message);
	}
}
