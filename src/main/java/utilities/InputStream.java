package utilities;

import java.io.*;
import java.util.Iterator;
import utilities.Strings;

/**
 * @file InputStream
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 11/01/2011
 * 
 * Class treatments files
 */
public class InputStream implements Iterable<String>{
	
	/**
     * Attribute that assigns a value to string delimiter.
     */
	private String delimiter="\n"; 
	
	/**
     * Attribute that assigns a value to string name.
     */
	private String name="";
	
	/**
	 * @fn InputStream().
	 * @brief Default constructor. 
	 */
	public InputStream(){}
	
	/**
	 * @fn InputStream(String nom).
	 * @brief Constructor with params.
	 * @param  String nom.
	 * @throws IOException.
	 */
	public InputStream(String nom) throws IOException{
		this.name = nom;
	}
	
	/**
	 * @fn InputStrema(String n, String del).
	 * @brief Constructor with all attributes of the class.
	 * @param String n.
	 * @param String del.
	 * @throws IOException.
	 */
	public InputStream(String n,String del) throws IOException{
		this.name = n;
		this.delimiter=del;
	}
	
	/**
	 * @fn Iterator<String> iterator().
	 * @brief A iterator with params.
	 * @return Iterator<String>
	 */
	public final Iterator<String> iterator(){
		return new IteradorFlujoEntrada(name,delimiter);	
	}
	
	/**
	 * @file IteradorFlujoEntrada
	 * @author Grupo 14, ISG2.
	 * @version 0.1
	 * @date 11/01/2011
	 * 
	 * Internal class the iterator.
	 */
	private class IteradorFlujoEntrada implements Iterator<String>{
		
		/**
		 * Attribute type BufferedReader.
		 */
		private BufferedReader bf;
		
		/**
		 * Attribute type Iterator<String>.
		 */
		private Iterator<String> itWords;
		
		/**
		 * Attribute type String.
		 */
		private String line, delims;
		
		/**
		 * @fn IteradorFlujoEntrada(String nom, String del).
		 * @brief Constructor with params
		 * @param String nom
		 * @param String del
		 */
		public IteradorFlujoEntrada(String nom, String del){
			if ("".equals(nom)){
				bf = new BufferedReader(new InputStreamReader(System.in));
			}else{
				try{
					bf = new BufferedReader(new FileReader(nom));
				}catch(FileNotFoundException e1){
					System.out.println("There is no file");
					bf = new BufferedReader(new InputStreamReader(System.in));
				}

				this.delims = del;

				try{
					line = bf.readLine();
					if(line!=null){ 
						itWords = Strings.separateElements(line, delims).iterator();
					}
				}catch(IOException e){ 
					e.printStackTrace();
				}
			}
		}
		
		/**
		 * @fn boolean hasNext().
		 * @brief Method that tells us if there following.
		 * @return boolean.
		 */
		public boolean hasNext(){ 	
			return itWords!=null  && itWords.hasNext(); 
		}
		
		/**
		 * @fn String next().
		 * @brief Method that says the following and updates.
		 * @return String.
		 */
		public String next(){
			String pal = itWords.next();
			
			if(!itWords.hasNext()){
				try{
					line = bf.readLine();
					if(line!=null){ 
						itWords = Strings.separateElements(line, delims).iterator();
					}else{
						bf.close();
					}
				}catch(IOException e){ 
					e.printStackTrace();
				}
			}

			return pal;
		}
		
		/**
		 * @fn void remove().
		 * @brief Default remove.
		 */
		public void remove(){}		
	}
}
