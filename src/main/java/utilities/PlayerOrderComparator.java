package utilities;

import java.awt.Point;
import java.util.*;
import models.Player;

/**
 * @file PlayerOrderComparator.
 * @author Grupo 14, ISG2.
 * @version 0.1.
 * @date 11/01/2012.
 * 
 *  This class compares the order of the players
 */

public class PlayerOrderComparator implements Comparator<Player>{
	
	/**
	 * Static attribute type List<Point>.
	 */
	private final List<Point> holePoints;
	
	/**
	 * Static attribute type Map<String, Integer>.
	 */
	private final Map<String, Integer> standings;
	
	/**
	 * @fn PlayerOrderComparator (List<Point> hPoints, Map<String, Integer> stand).
	 * @brief Compares the distances of the balls to see the turn of the players.
	 * @param List<Point> hPoints.
	 * @param Map<String, Integer> stand.
	 * 
	 */
	public PlayerOrderComparator (List<Point> hPoints, Map<String, Integer> stand){
		this.holePoints = hPoints;
		this.standings = stand;
	}
	
	/**
	 * @fn int compare (Player a, Player b).
	 * @brief Comparator of two players and return the furthest.
	 * @return int.
	 */
	public int compare(Player a, Player b){
		Double minA = minimumDistanceToHole(a);
		Double minB = minimumDistanceToHole(b);		
		int result = minA.compareTo(minB);//El m�s lejano al hoyo 1�
		
		if (result == 0){
			//El m�s lejano en  los standings
			Integer standingA = standings.get(a.getPlayerName());
			Integer standingB = standings.get(b.getPlayerName());
			result = standingA.compareTo(standingB);
		}
		
		//Random
		//Como el compare necesita numeros menores iguales o mayores a 0, uso esa formulita
		if (result == 0){	
			result = (new Random()).nextInt(3) - 1;
		}
		
		return -result;
	}
	
	/**
	 * @fn double minimumDistanceToHole(Player player).
	 * @param Player player.
	 * @return double.
	 */
	private double minimumDistanceToHole(Player player){
		Collections.sort(holePoints, new PlayerDistanceComparator(player));
		Point minDistancePoint = holePoints.get(holePoints.size() - 1);
		return player.getBallPosition().distance(minDistancePoint);
	}
}
