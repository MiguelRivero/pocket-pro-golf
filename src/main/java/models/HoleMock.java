package models;

import java.awt.Point;
import java.io.IOException;
import java.util.List;

/**
 * @file HoleMock
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 15/12/2011
 */
public class HoleMock extends Hole{
	
	/**
	 * @fn HoleMock(Integer numberOfFairways).
	 * @brief Constructor of the class with params.
	 * @param Integer numberOfFairways.
	 * @throws IOException
	 */
	public HoleMock(Integer numberOfFairways) throws IOException{
		super(numberOfFairways);
	}

	/**
	 * @fn HoleMock(List<String> cardNames).
	 * @brief Constructor of the class with params.
	 * @param List<String> cardNames.
	 */
	public HoleMock(List<String> cardNames){
		super(cardNames);
		wind= new Point(1, 0);
	}	
}
