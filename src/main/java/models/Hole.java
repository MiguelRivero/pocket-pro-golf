package models;

import java.awt.Point;
import java.io.IOException;
import java.util.*;
import cards.HoleCard;
import utilities.*;

/**
 * @file Hole
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 15/12/2011
 */
public class Hole{	
	
	/**
	 * Attribute that tells us the list of cards used.
	 */
	private List<String> usedCards;
	
	/**
	 * Attribute with the complete hole to play.
	 */
	private List<List<String>> completeHole;
	
	/**
	 * Attribute that tells us about the wind.
	 */
	protected Point wind;
	
	/**
	 * Attribute with the hole parts.
	 */
	private List<HoleCard> segments;

	/**
	 * Attribute with the actual hole part.
	 */
	private HoleCard actualSegment;

	/**
	 * Attribute with the par of the hole.
	 */
	private Integer par;
	
	/**
	 * @fn Hole(Integer numberOfFairways).
	 * @brief Constructor of the class with params.
	 * @param Integer numberOfFairways.
	 * @throws IOException.
	 */
	public Hole(Integer numberOfFairways) throws IOException{
		if (numberOfFairways < 0 || numberOfFairways > 5){
			throw new IllegalArgumentException("The range of numFairways must be [0,5]");
		}else{
			segments = new ArrayList<HoleCard>();
			segments.add(Factory.newTee());
			for (int i = 0; i < numberOfFairways; i++)
				segments.add(Factory.newFairway());
			segments.add(Factory.newGreen());
			actualSegment = segments.get(0);
			completeHole = new ArrayList<List<String>>();
			usedCards = new ArrayList<String>();
			setWind();
			joinCards();
			calculatePar();
		}
	}
	
	/**
	 * @fn Hole(List<String> cardNames).
	 * @brief Constructor of the class with params.
	 * @param List<String> cardNames: a list with the hole cards that compose the hole.
	 * @throws IOException.
	 */
	public Hole(List<String> cardNames){
		segments = new ArrayList<HoleCard>();
		
		try {
			for (int i = 0; i < cardNames.size(); i++){
				if (i == 0)
					segments.add(Factory.newTee(cardNames.get(i)));
				else if (i == cardNames.size() - 1)
					segments.add(Factory.newGreen(cardNames.get(i)));
				else
					segments.add(Factory.newFairway(cardNames.get(i)));
			}
			completeHole = new ArrayList<List<String>>();
			usedCards = new ArrayList<String>();
			setWind();
			joinCards();
			calculatePar();
		}catch (IOException e){
			throw new IllegalArgumentException("Card not found.");			
		}
	}

	/**
	 * @fn void setWind().
	 * @brief Random wind.
	 */
	public final void setWind(){
		Integer x = 1, y = 1, dice = Dice.roll();
		
		if (dice == 2){
			x = 1;
			y = 0;
		}else if (dice == 3){
			x = 1;
			y = -1;
		}else if (dice == 4){
			x = -1;
			y = -1;
		}else if (dice == 5){
			x = -1;
			y = 0;
		}else if (dice == 6){
			x = -1;
			y = 1;
		}
		
		this.wind = new Point(x, y);
	}
	
	/**
	 * @fn Point getWind()
	 * @brief Getter of the property <tt>wind</tt>
	 * @return Returns the wind direction in a point type
	 */
	public final Point getWind(){
		return wind;
	}

	/**
	 * @fn List<HoleCard> getSegments().
	 * @brief Getter of the property <tt>segments</tt>.
	 * @return A list of holeCard.
	 */
	public final List<HoleCard> getSegments(){
		return segments;
	}
	
	/**
	 * @fn HoleCard getActualSegment().
	 * @brief Getter of the property <tt>actualSegment</tt>.
	 * @return A HoleCard type.
	 */
	public final HoleCard getActualSegment(){
		return actualSegment;
	}
	
	/**
	 * @fn void joinCards().
	 * @brief Join the segments in a only list.
	 */	
	private void joinCards(){
		for (HoleCard h : segments){
			completeHole.addAll(h.getHoleSegment());
			usedCards.add(h.getHoleCardName());
		}
	}
	
	/**
	 * @fn List<List<String>> getHole().
	 * @brief Getter of the property <tt>hole</tt>
	 * @return List<List<String>>.
	 */	
	private List<List<String>> getHole(){
		return completeHole;
	}	
	
	/**
	 * @fn List<String> getUsedCardsNames().
	 * @brief Getter of the property <tt>usedCards</tt>.
	 * @return List<String> with the used cards.
	 */
	private List<String> getUsedCardsNames(){
		return usedCards;
	}
	
	/**
	 * @fn String toString().
	 * @brief A hole with String format.
	 * @return String.
	 */
	public final String toString(){
		StringBuffer buffer = new StringBuffer();
		List<List<String>> aux = getHole();
		buffer.append("Card name: " + getUsedCardsNames().toString() + "\n");
		
		for(int i = aux.size() - 1; i >= 0; i--){
			List<String> list = aux.get(i);
			for(String surface : list){
				buffer.append(String.format("%4s",surface));
			}
			buffer.append("\n");
		}
		
		return buffer.toString();
	}
	
	/**
	 * @fn Point getRebound (Point actualPosition).
	 * @brief Returns the position where the ball will bounce after the trees.
	 * @param Point actualPosition.
	 * @return Point.
	 */
	public final Point getRebound (Point actualPosition){
		Point point = new Point();		
		List<List<String>> holeSegment = getHole();
		Map<Double, Point> reboundMap = new HashMap<Double, Point>();
		
		//Recoge todas las coordenadas de todos los rebotes del hoyo
		for (int i = 0; i < actualPosition.getY()+1; i++){
			List<String> line = holeSegment.get(i);
			if (line.contains(Surfaces.getRebound())){
				int x = line.indexOf(Surfaces.getRebound());
				int y = holeSegment.indexOf(line);
				Point rebound = new Point(x, y);
				Double distanceToMe = actualPosition.distance(rebound);
				if (!reboundMap.containsKey(distanceToMe)){
					reboundMap.put(distanceToMe, point);
				}
			}
		}
		
		//Busca el que tenga menor distancia
		Set<Double> distanceSet = reboundMap.keySet();
		SortedSet<Double> distanceSet2 = new TreeSet<Double>();
		
		for(Double d:distanceSet){
			distanceSet2.add(d);
		}
		point = reboundMap.get(distanceSet2.last());
		
		return point;
	}	
	
	/**
	 * @fn String getSurface(Point coordinates).
	 * @brief Returns the surface where the ball is and throw Exception if the ball goes out of field.
	 * @param Point coordinates.
	 * @return Surface where the ball is in this moment.
	 */
	public final String getSurface(Point coordinates){
		String surface="";
		
		if (coordinates == null){
			throw new IllegalArgumentException("Coordinates are null on HoleCard class");
		}else if(isOutOfBounds(coordinates)){
			throw new OutOfFieldException("You are out of the field");
		}else{
			surface = getHole().get(coordinates.y).get(coordinates.x);
		}
	
		return surface;
	}
	
	/**
	 * @fn Boolean isNextToHole(Point point).
	 * @brief Check if the point pased for param is the surface next to hole.
	 * @param Point point.
	 * @return Boolean.
	 */
	public final Boolean isNextToHole(Point point){
		String surface = "";
		
		if (point == null){
			throw new IllegalArgumentException("Point are null on Green class");
		}
	//	else if (!(point instanceof Point))
	//		throw new ClassCastException("Not a Point class or child on Green class");
		else if( point.getY()> getHole().size() && point.getX() > getHole().get(point.y).size()){
			throw new OutOfFieldException("You are out of the field");
		}else{
			surface = getSurface(point);
		}
		
		return surface.compareToIgnoreCase(Surfaces.getNextToHole()) == 0;
	}
	
	/**
	 * @fn Point getStartPoint().
	 * @brief Returns the starting position of the hole.
	 * @return Point. 
	 */
	public final Point getStartPoint(){
		Point point = new Point();		
		List<List<String>> holeSegment = getHole();
		Boolean flag = false;
		
		for (int i = 0; i < holeSegment.size() && !flag;i++){
			List<String> line = holeSegment.get(i);
			if (line.contains(Surfaces.getTee())){
				int x = line.indexOf(Surfaces.getTee());
				int y = holeSegment.indexOf(line);
				point.setLocation(x, y);
				flag = true;
			}
		}
		
		return point;			
	}
	
	/**
	 * @fn List<Point> getNextToHoleList()
	 * @brief Returns the spaces which are next to hole
	 * @return List<Point>
	 */
	public final List<Point> getNextToHoleList(){
		List<Point> pointList = new ArrayList<Point>();
		List<List<String>> holeSegment = getHole();
		
		for (int i = 0; i < holeSegment.size();i++){
			List<String> line = holeSegment.get(i);
			if (line.contains(Surfaces.getNextToHole())){
				for (int j = 0; j < line.size(); j++){
					if (line.get(j).equals(Surfaces.getNextToHole())){
						Point point = new Point();
						int x = j;
						int y = i;
						point.setLocation(x, y);
						pointList.add(point);
					}
				}
			}
		}
		
		return pointList;
	}

	/**
	 * @fn Integer calculatePar().
	 * @brief Calculate the par of the hole depends of the number of holeCards.
	 */
	private void calculatePar(){
		Integer p = 0;
		
		if (usedCards.size() == 2){
			p = 3;
		}else if (usedCards.size() == 3 || usedCards.size() == 4){
			p = 4;
		}else if (usedCards.size() == 5){
			p = 5;
		}
		
		this.par = p;
	}

	/**
	 * @fn Integer getPar().
	 * @brief Getter of the property <tt>par</tt>.
	 * @return Integer: Returns the par of the hole.
	 */
	public Integer getPar(){		
		return par;
	}

	/**
	 * @fn Boolean isOutOfBounds(Point position).
	 * @brief Check if the ball go to out of bounds.
	 * @param Point position.
	 * @return Boolean.
	 */
	public Boolean isOutOfBounds(Point position){		
		return position.x < 0 || position.y < 0 || position.x > 7 || position.y > getHole().size() - 1;
	}

	/**
	 * @fn String toString(Point playerLocation).
	 * @brief Returns the Hole as a String format and print out the hole with the player's position included.
	 * @param Point playerLocation.
	 * @return String.
	 */
	public String toString(Point playerLocation){
		//Imprime el hoyo con la posici�n del jugador inclu�da
		StringBuffer buffer = new StringBuffer();
		List<List<String>> aux = getHole();
		System.out.println("Wind: (" + getWind().x + ", " + getWind().y + ")");
		buffer.append("Card name: " + getUsedCardsNames().toString() + "\n");
		
		for(int i = aux.size() - 1; i >= 0; i--){
			List<String> list = aux.get(i);
			for(int j = 0; j < list.size(); j++){
				String surface = list.get(j);
				String format = "%4s";
				if (playerLocation.x == j && playerLocation.y == i){
					surface = "[" + list.get(j) + "]";
					format = "%5s";
				}
				else if (playerLocation.y == i && playerLocation.x + 1 == j)
					format = "%3s";
				buffer.append(String.format(format,surface));
			}
			buffer.append("  | "+ i +"\n");
		}
		
		return buffer.toString();
	}
	
	/**
	 * @fn Point getNearestHole(Player player).
	 * @brief Returns the nearest next to hole of the player passed for params.
	 * @param Player player.
	 * @return Point.
	 */
	public Point getNearestHole(Player player){
		List<Point> holeList = getNextToHoleList();
		Collections.sort(holeList, new PlayerDistanceComparator(player));
		return holeList.get(holeList.size() - 1);
	}

	public Integer lenght() {
		return getHole().size();
	}	
}
