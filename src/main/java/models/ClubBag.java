package models;

import java.io.IOException;
import java.util.*;
import utilities.*;

/**
 * @file ClubBag
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 15/12/2011
 */
public class ClubBag{	
	
	/**
	 * Attribute with the list of kinds of clubs.
	 */	
	private final List<Club> clubBag = new LinkedList<Club>();
	
	/**
	 * @fn ClubBag().
	 * @brief Constructor of the class.
	 */		
	public ClubBag(){		
		InputStream fe = new InputStream();
		
		try {
			fe = new InputStream("files/clubs.txt");
		}catch(IOException e){
			e.printStackTrace();
		}		
		
		ArrayList<String> lines = new ArrayList<String>();

		for(String line : fe){
			lines.add(line);
		}
		for(String v:lines){
			ArrayList<String> separeClubName = Strings.separateElements(v, ":");
			ArrayList<String> vector = Strings.separateElements(separeClubName.get(1), ",");
			Club c = Factory.newClub(separeClubName.get(0), Integer.valueOf(vector.get(0)), 
					Integer.valueOf(vector.get(1)),Integer.valueOf(vector.get(2)), 
					Integer.valueOf(vector.get(3)), Integer.valueOf(vector.get(4)),vector.get(5));
			clubBag.add(c);			
		}
	}
	
	/**
	 * @fn Club getClub(String clubName).
	 * @brief Return a club with the name pased on parameter.
	 * @param String clubName.
	 * @return Club.
	 */	
	public final Club getClub(String clubName){
		Club club = Factory.newClub();
		
		for(Club c:getClubBag()){
			if(c.getClubName().equals(clubName)){
				club = c;
			}
		}
		
		return club;
	}
	
	/**
	 * @fn List<Club> getClubBag().
	 * @brief Getter of the property <tt>clubBag</tt>
	 * @return List<Club>.
	 */
	public final List<Club> getClubBag(){
		return this.clubBag;
	}	
	
	/**
	 * @fn String toString(Player player)
	 * @brief Club bag with a string format with the skilltokens of the playes passed for parameters.
	 * @return String.
	 */
	public String toString(Player player){
		List<Club> list = getClubBag();
		StringBuffer ret = new StringBuffer();
		Integer i=1;
		for(Club c:list){
			ret.append(i+".- "+c.getClubName()+": "
					+c.getDescription()+"\n"+"    Skill: "
					+player.getSkillTokenInClub(c.getClubName())+"\n\n");
			i++;
		}
		return ret.toString();
	}
}
