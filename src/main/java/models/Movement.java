package models;

import java.awt.*;
import mains.*;
import utilities.*;

/**
 * @file Movement
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 15/12/2011
 */
public class Movement{	
	
	/**
	 * @fn Movement(Hole h).
	 * @brief Constructor of Movement.
	 * @param Hole h.
	 */
	public Movement(Hole h){
		this.hole = h;
		this.wind = new Point(getHole().getWind());
		this.direction = new Point(0,1);
		this.theBallIsIn = false;
	}	
	
	/**
	 * Attribute theBallIsIn in game.
	 */
	private Boolean theBallIsIn;
	
	/**
	 * @fn Boolean getTheBallIsIn().
	 * @brief Getter of the property <tt>theBallIsIn</tt>.
	 * @return Boolean.
	 */
	public final Boolean getTheBallIsIn(){
		return this.theBallIsIn;
	}
	
	/**
	 * @fn void setTheBallIsIn(Boolean b).
	 * @brief Setter of the property <tt>theBallIsIn</tt>.
	 * @param Boolean b.
	 */
	public final void setTheBallIsIn(Boolean b){
		this.theBallIsIn = b;
	}
	
	/**
	 * Attribute hole in game.
	 */
	private final Hole hole;	
	
	/**
	 * @fn Hole getHole().
	 * @brief Getter of the property <tt>hole</tt>.
	 * @return Hole.
	 */
	public final Hole getHole(){
		return this.hole;
	}
	
	/**
	 * Attribute wind in the hole in game.
	 */
	private final Point wind;
	
	/**
	 * @fn Point getWind().
	 * @brief Getter of the property <tt>wind</tt>.
	 * @return Point.
	 */
	public final Point getWind(){
		return this.wind;
	}
	
	/**
	 * Attribute direction.
	 */
	private final Point direction;
	
	/**
	 * @fn Point getDirection().
	 * @brief Getter of the property <tt>direction</tt>.
	 * @return Point.
	 */
	public final Point getDirection(){
		return this.direction;
	}
	
	/**
	 * @fn setDirection(Point dir).
	 * @brief Setter of the property <tt>direction</tt>.
	 * @param Point dir.
	 */
	public final void setDirection(Point dir){
		this.direction.setLocation(dir);
	}
	
	/**
	 * @fn Point calculateGroundAndSandMovement(Point actualPosition, Integer progress).
	 * @brief The ball has dropped in the sand and moves 1 square for every 2 rolls that fit ground.
	 * @param Point actualPosition.
	 * @param Integer progress.
	 * @return Point with the next position.
	 */	
	//La bola ha caido en la arena, y avanza 1 casilla por cada 2 tiradas de ground que le queden.
	public final Point calculateGroundAndSandMovement(Point actualPosition, Integer progress){
		Point point = new Point(actualPosition);
		int i=0;
		
		while (i<progress){
			point.setLocation(ManipulatePoints.sumPoints(point, getDirection()));
			i++;
		}
		
		return point;
	}
	
	/**
	 * @fn Point calculateGroundMovement(Point actualPosition, String clubName, Throw strike).
	 * @brief Generate next position in a ground throw.
	 * @param Point actualPosition.
	 * @param String clubName.
	 * @param Throw strike.
	 * @return Point with the next position.
	 */
	public final Point calculateGroundMovement(Point actualPosition, String clubName, Throw strike){
		Point point = new Point(actualPosition);
		int progress = strike.getProgress();
		int i=0;
		Boolean stopMovement = false;
		Boolean isNextToHole = false;
		
		if ( ( getHole().getSurface(point).equals(Surfaces.getRough()) 
				|| getHole().getSurface(point).equals(Surfaces.getSand()) ) 
				&& clubName.equals(Clubs.getWedge()) ){
			progress--;
		}
		
		while (i<progress && !stopMovement){
			point.setLocation(ManipulatePoints.sumPoints(point, getDirection()));
			if(!getHole().isOutOfBounds(point)){
				if (getHole().getSurface(point).equals(Surfaces.getSand()) && !getHole().getSurface(actualPosition).equals(Surfaces.getSand())){
					//Se llama en game al metodo calculateGroundAndSandMovement y termina el movimiento
					stopMovement = true;
				}else if (getHole().getSurface(point).equals(Surfaces.getWater())){
					//Penaliza con un golpe, termina el movimiento y devuelve la posicion en el agua
					stopMovement = true;				
				}else if (getHole().getSurface(point).equals(Surfaces.getTree())){
					//Penaliza con un golpe, termina el movimiento y devuelve la posicion en el arbol
					stopMovement = true;
				}else if ( getHole().getSurface(point).equals(Surfaces.getNextToHole()) 
							&& i>1  && !isNextToHole){
					//Si pasa por un next to hole y va a pasarse del hoyo, que siga palante
					isNextToHole = true;
				}else if ( getHole().getSurface(point).equals(Surfaces.getNextToHole()) 
							&& i==1  && !isNextToHole){
					//Si cae en next to hole y le queda un movimiento, entra directamente en el hoyo y se a�ade un golpe
					stopMovement = true;
					setTheBallIsIn(true);
				}
			}else
				stopMovement = true;
			i++;
		}
		
		if (!stopMovement){
			Point pointValue = new Point(0,0);
			
			pointValue.setLocation(getDoublesOrTriples(strike));
			
			point.setLocation(ManipulatePoints.sumPoints(point,pointValue));
		}
		
		return point;
	}	
	
	/**
	 * @fn Point calculateAirMovement(Point actualPosition, String clubName, Throw strike).
	 * @brief Generate next position in an air throw.
	 * @param Point actualPosition.
	 * @param String clubName.
	 * @param Throw strike.
	 * @return Point with the next position.
	 */
	public final Point calculateAirMovement(Point actualPosition, String clubName, Throw strike){
		Point point = new Point(actualPosition);
		int progress = strike.getProgress();
		int i=0;
		Boolean stopMovement = false;
		
		if (getHole().getSurface(point).equals(Surfaces.getTee())){
			progress++;
		}
		
		while (i<progress && !stopMovement){
			point.setLocation(ManipulatePoints.sumPoints(point, getDirection()));
			if(!getHole().isOutOfBounds(point)){
				if (getHole().getSurface(point).equals(Surfaces.getTree())) {
					//Penaliza con un golpe, termina el movimiento y devuelve la posicion en el arbol
					stopMovement = true;
				}
			}else
				stopMovement = true;
			i++;
		}
		
		if (!stopMovement){
			Point pointValue = new Point(0,0); 
			
			pointValue.setLocation(getDoublesOrTriples(strike));
			
			point.setLocation(ManipulatePoints.sumPoints(point,pointValue));
		}		
		
		return point;
	}	
	
	/**
	 * @fn Point calculateFirstAndSecondAirMovement(Point actualPosition, Point aim, String clubName, Throw strike).
	 * @brief Generate next position in the first and second movement.
	 * @param Point actualPosition.
	 * @param Point aim.
	 * @param String clubName.
	 * @param Throw strike.
	 * @return Point with the next position.
	 */
	public final Point calculateFirstAndSecondAirMovement(Point actualPosition, Point aim, String clubName, Throw strike){
		Point point = new Point(actualPosition);
		int progress = strike.getProgress();
		int i=0;
		Boolean stopMovement = false;
		
		if (getHole().getSurface(point).equals(Surfaces.getTee())){
			progress++;
		}
		
		while (i<progress && !stopMovement){
			point.setLocation(ManipulatePoints.sumPoints(point, getDirection()));
			if(!getHole().isOutOfBounds(point)){
				if (getHole().getSurface(point).equals(Surfaces.getTree())) {
					//Penaliza con un golpe, termina el movimiento y devuelve la posicion en el arbol
					stopMovement = true;
				}
			}else
				stopMovement = true;
			i++;
		}
		
		if (!stopMovement){			
			Point pointValue = new Point(0,0);
			
			pointValue.setLocation(getDoublesOrTriples(strike));
			
			point.setLocation(ManipulatePoints.sumPoints(point,pointValue));
			
			point.setLocation(ManipulatePoints.sumPoints(point,aim));
		}
		
		return point;
	}
	
	/**
	 * @fn Point getValidPositionOfOutOfBoundPenalty(Point actualPosition).
	 * @brief Generate a valid position when the ball go to out of bounds.
	 * @param Point actualPosition.
	 * @return Point with the valid position.
	 */
	public final Point getValidPositionOfOutOfBoundsPenalty(Point actualPosition){
		Point point = new Point(actualPosition);
		int x = point.x;
		int y = point.y;
		String directionOfPenalty = "";
		Integer limitY = getHole().lenght();
		Integer limitX = 6;
		//carta de 7x9, 0-6 y 0-8		
		
		if (point.getX()<0 && point.getY()<limitY){//si salgo por la izda y no arriba
			x = 0;
			directionOfPenalty = "left";
		}else if(point.getX()<0 && point.getY()>limitY){//si salgo por la izda y por arriba
			x = 0;
			y = limitY;
			directionOfPenalty = "upLeft";
		}else if(point.getX()>limitX && point.getY()<limitY){//si salgo por la drcha y no arriba
			x = limitX;
			directionOfPenalty = "rigth";
		}else if(point.getX()>limitX && point.getY()>limitY){//si salgo por la drcha y por arriba
			x = limitX;
			y = limitY;
			directionOfPenalty = "upRigth";
		}else if(point.getY()>limitY && (point.getX()>0 && point.getX()<limitX)){//si salgo por arriba y no drcha ni izda
			y = limitY;
			directionOfPenalty = "up";
		}
		
		point.setLocation(x,y);
		
		//Mira por donde sale la bola para ponerla en la posicion mas cercana valida
		if (getHole().getSurface(point).equals(Surfaces.getTree()) || getHole().getSurface(point).equals(Surfaces.getWater())){
			Point subtractPoint = new Point();
			
			if ("left".equals(directionOfPenalty)){
				subtractPoint.setLocation(1,0);
			}else if ("rigth".equals(directionOfPenalty)){
				subtractPoint.setLocation(-1,0);
			}else if ("upLeft".equals(directionOfPenalty)){
				subtractPoint.setLocation(1,-1);
			}else if ("upRigth".equals(directionOfPenalty)){
				subtractPoint.setLocation(-1,-1);
			}
			
			while (getHole().getSurface(point).equals(Surfaces.getTree()) || getHole().getSurface(point).equals(Surfaces.getWater())){ 
				point.setLocation(ManipulatePoints.sumPoints(point,subtractPoint));
			}
		}
		
		return point;
	}
	
	/**
	 * @fn Point getValidPositionOfWaterPenalty(Point actualPosition).
	 * @brief Generate a valid position when the ball falls into the water.
	 * @param Point actualPosition.
	 * @return Point with the valid position.
	 */
	public final Point getValidPositionOfWaterPenalty(Point actualPosition){
		Point point = new Point(actualPosition);
		Point subtractPoint = new Point();
		
		//Mira si es agua en horizontal, es decir, si en el medio hay agua,
		//es que es en horizontal y mueve la bola hacia abajo hasta una posicion valida
		if ( getHole().getSurface(new Point(3,point.y)).equals(Surfaces.getWater())){
			subtractPoint.setLocation(0,-1);
			while (getHole().getSurface(point).equals(Surfaces.getTree()) || getHole().getSurface(point).equals(Surfaces.getWater())){ 
				point.setLocation(ManipulatePoints.sumPoints(point,subtractPoint));
			}
		}else{
			//Si no es en horizontal, es un lago en vertical y mueve la bola hacia la derecha o izquierda hasta una posicion valida
			if (point.x < 3)
				subtractPoint.setLocation(1,0);
			else
				subtractPoint.setLocation(-1,0);			
			
			while (getHole().getSurface(point).equals(Surfaces.getTree()) || getHole().getSurface(point).equals(Surfaces.getWater())){ 
				point.setLocation(ManipulatePoints.sumPoints(point,subtractPoint));
			}
		}
		return point;
	}
	
	/**
	 * @fn Point getValidPositionOfTreePenalty(Point actualPosition).
	 * @brief Generate a valid position when the ball falls into the trees.
	 * @param Point actualPosition.
	 * @return Point with the valid position.
	 */
	public final Point getValidPositionOfTreePenalty(Point actualPosition){
		return getHole().getRebound(actualPosition);
	}
	
	/**
	 * @fn getDoublesOrTriples(Throw st).
	 * @brief Return the point plus the wind when is the roll is doubles or triples.
	 * @param Throw st.
	 * @return Point with the manipulate wind.
	 */
	public final Point getDoublesOrTriples(Throw st){
		Point pointValue = new Point(0,0);
		
		if (st.getRollTriples()){
			pointValue.setLocation(ManipulatePoints.reversePoint(getWind()));
		}else if (st.getRollDoubles()){
			pointValue.setLocation(getWind());
		}
		
		return pointValue;
	}
}
