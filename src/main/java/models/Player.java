package models;

import java.awt.Point;
import java.util.*;

/**
 * @file Player
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 15/12/2011
 */
public class Player{
	
    /**
	 * Attribute that tells the player's name.
	 */
	private String playerName;
	
    /**
	 * Attribute that will contain the number of "skill tokens" in each suit.
	 */
	private final Map<String,Integer> skillTokensInClubs;
	
	/**
	 * Attribute with the position of the player ball.
	 */
	private Point ballPosition;
	
	/**
	 * Attribute with the number of player's hits.
	 */
	private Integer hits;
	
	/**
	 * @fn Player(String name).
	 * @brief Constructor of Player with params.
	 * @param String name.
	 * 
	 * Constructor of player that puts the player name and initializes the map of club and other attributes.
	 */
	public Player(String name){
		this.playerName = name;		
		this.ballPosition = new Point();
		this.hits = 0;
		this.skillTokensInClubs = new HashMap<String, Integer>();
		createSkillTokenInClub();
	}
	
	/**
	 * @fn String getPlayerName().
	 * @brief Getter of the property <tt>playerName</tt>.
	 * @return String.
	 */	
	public String getPlayerName(){
		return this.playerName;
	}
	
	/**
	 * @fn void setPlayerName(String playerName).
	 * @brief Setter of the property <tt>playerName</tt>.
	 * @param String playerName.
	 */	
	public void setPlayerName(String pName){
		this.playerName = pName;
	}
		
    /**
	 * @fn Integer getSkillTokenInClub(String club).
	 * @brief Method is passed a club and returns the number of "skill tokens" in this club.
	 * @param String club.
	 * @return Integer.
	 */
	public Integer getSkillTokenInClub(String club){
		return this.skillTokensInClubs.get(club);
	}
	
	/**
	 * @fn void createSkillTokenInClub().
	 * @brief Method to initialize the Map that stores the number of "skill tokens".
	 */
	public void createSkillTokenInClub(){
		this.skillTokensInClubs.put("Wedge",0);
		this.skillTokensInClubs.put("Driver",0);
		this.skillTokensInClubs.put("Putter",0);
		this.skillTokensInClubs.put("3Iron",0);
		this.skillTokensInClubs.put("5Iron",0);		
		this.skillTokensInClubs.put("7Iron",0);
		this.skillTokensInClubs.put("9Iron",0);
	}
	
	/**
	 * @fn void addSkillTokenInClub(String club,Integer numSkillToken).
	 * @brief Method that introduces a number of skill tokens on the indicated "club".
	 * @param String club.
	 * @param Integer numSkillToken.
	 */
	public void addSkillTokenInClub(String club, Integer numSkillToken){
		this.skillTokensInClubs.put(club, getSkillTokenInClub(club) + numSkillToken);
	}
	
    /**
	 * @fn void addTokenInClub(String club).
	 * @brief Add a skillToken to the indicated "club".
	 * @param String club.
	 */
	public void addSkillTokenInClub(String club){
		this.skillTokensInClubs.put(club, getSkillTokenInClub(club)+1);
	}
	
    /**
	 * @fn void removeTokenInClub(String club).
	 * @brief Remove one skill token of the selected club.
	 * @param String club.
	 * 
	 * Remove a "skill token" of the "club" indicated.
	 */
	public void removeTokenInClub(String club) {
		if(getSkillTokenInClub(club)>0){
			this.skillTokensInClubs.put(club, getSkillTokenInClub(club)-1);
		}else{
			System.out.println("You have no token to remove in the club: " +club);
		}
	}
	
	/**
	 * @fn void removeToken(String club,Integer numSkillToken).
	 * @param String club.
	 * @param Integer numSkillToken.
	 * 
	 * Remove the number of "skill token" parameter received in the club indicated.
	 */
	public void removeToken(String club, Integer numSkillToken){
		if( (getSkillTokenInClub(club)>=numSkillToken) && numSkillToken<6 ){
			this.skillTokensInClubs.put(club, getSkillTokenInClub(club)-numSkillToken);
		}else{
		    System.out.println("You don`t have enough tokens to remove in the club: " + club);
		}
	}
	
	/**
	 * @fn Point getBallPosition().
	 * @brief Getter of the property <tt>ballPosition</tt>.
	 * @return Point.
	 */
	public Point getBallPosition(){
		return this.ballPosition;
	}
	
    /**
	 * @fn void setBallPosition(Point ballPosition).
	 * @brief Setter of the property <tt>ballPosition</tt>.
	 * @param Point ballPosition.
	 */
	public void setBallPosition(Point bPosition){
		this.ballPosition = bPosition;
	}    
	
	/**
	 * @fn Integer getHits().
	 * @brief Getter of the property <tt>hits</tt>.
	 * @return Integer.
	 */
	public Integer getHits(){
		return this.hits;
	}
	
	/**
	 * @fn void setHits(Integer h).
	 * @brief Setter of the property <tt>hits</tt>.
	 * @param Integer h.
	 */	
	public void setHits(Integer h){
		this.hits = h;
	}
	
	/**
     * @fn void increaseHit().
     * @brief Increase hits property.
     */
	public void increaseHit(){
		setHits(getHits()+ 1);
	}	

    /**
     * @fn void resetPlayer().
     * @brief Reset the attributes of the player.
     */
	public void resetPlayer(){
		setHits(0);
	}

	/**
     * @fn boolean canMulligan().
     * @brief Check if the sum of skill tokens is > 3 to do mulligan.
     * @return boolean.
     */
	public boolean canMulligan() {
		return skillTokensAmount() >= 3;
	}
	
	/**
     * @fn int skillTokensAmount().
     * @brief Sum al the skill tokens in all clubs.
     * @return int.
     */
	public int skillTokensAmount(){
		Integer skillTokens = 0;
		
		for (String club : getSkillTokensInClubs().keySet())
			skillTokens += getSkillTokensInClubs().get(club);
		
		return skillTokens;		
	}

	/**
     * @fn Map<String, Integer> getSkillTokensInClubs().
     * @brief Getter of the property <tt>skillTokensInClubs</tt>.
     * @return Map<String, Integer>.
     */
	private Map<String, Integer> getSkillTokensInClubs() {
		return this.skillTokensInClubs;
	}
}
