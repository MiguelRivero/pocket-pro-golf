package models;

import java.util.List;

/**
 * @file Tournament
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 12/12/2011
 */
public class Tournament{	
	
	/**
	 * Attribute that tells the name of the tournament.
	 */
	private String tournamentName;		
	
	/**
	 * Attribute that tells us the list of holes.
	 */
	private List<Hole> holes;	
	
	/**
	 * Attribute that tells the player list.
	 */
	private List<Player> players;	
	
	/**
	 * @fn Tournament(String n, List<Player> p, List<Hole> h).
	 * @brief Constructor of Tournament.
	 * @param n name of the tournament.
	 * @param p list of players.
	 * @param h list of holes.
	 */	
	public Tournament(String n, List<Player> p, List<Hole> h){
		tournamentName = n;			
		players = p;
		holes = h;	
	}
	
	/**
	 * @fn String getTournamentName().
	 * @brief Getter of the property <tt>tournamentName</tt>.
	 * @return name of the tournament.
	 */
	public final String getTournamentName(){
		return tournamentName;
	}

	/**
	 * @fn String setTournamentName(String tournamentName).
	 * @brief Setter of the property <tt>tournamentName</tt>.
	 * @param tournamentName  Name of Tournament.
	 */
	public final void setTournamentName(String tName){
		this.tournamentName = tName;
	}

	/**
	 * @fn List<Hole> getHoles().
	 * @brief Getter of the property <tt>holes</tt>.
	 * @return List<Hole>.
	 */
	public final List<Hole> getHoles(){
		return holes;
	}
	
	/**
	 * @fn setHoles(List<Hole> holes).
	 * @brief Setter of the property <tt>holes</tt>.
	 * @param holes List<Hole>.
	 */	
	public final void setHoles(List<Hole> h){
		this.holes = h;
	}
	
	/**
	 * @fn List<Player> getPlayers().
	 * @brief Getter of the property <tt>players</tt>.
	 * @return List of players.
	 */	
	public final List<Player> getPlayers(){
		return players;
	}
	
	/**
	 * @fn setPlayers(List<Player> p)
	 * @brief Setter of the property <tt>players</tt>.
	 * @param List<Player> p.
	 */	
	public final void setPlayers(List<Player> p){
		this.players = p;
	}	
	
	/**
	 * @fn String toString().
	 * @brief Returns a string with the tournament name and number of holes.
	 * @return String.
	 */	
	public final String toString(){
		StringBuffer buff = new StringBuffer();
		buff.append("\n\nThe tournament name is: "+getTournamentName()+
				"\n-----------------------------------------------------\nThe List of players is: {");
		
		for(Player p: players){
			buff.append(p.toString()+", ");
		}
		
		buff.append("}");
		String dev = buff.toString();
		
		return dev;
	}
}
