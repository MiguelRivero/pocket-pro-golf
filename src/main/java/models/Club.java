package models;

/**
 * @file Club
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 15/12/2011
 */
public class Club{
	
	/**
	 * Attribute tells name of the club.
	 */	
	private String clubName;
	
	/**
	 * Attribute with the number of Air rolls.
	 */	
	private Integer air;
	
	/**
	 * Attribute with the number of Ground rolls.
	 */	
	private Integer ground;	
	
	/**
	 * Attribute with the penalty on the tee. 
	 */	
	private Integer tee;
	
	/**
	 * Attribute with the penalty of Air in the rough.
	 */	
	private Integer roughAir;
	
	/**
	 * Attribute with the penalty of Ground in the rough.
	 */	
	private Integer roughGround;	
	
	/**
	 * Attribute with the description of the club.
	 */	
	private String description;	
	
	/**
	 * @fn Club().
	 * @brief Default constructor of the class.
	 */
	public Club(){}
	
	/**
	 * @fn Club(String name, Integer a, Integer g, Integer t, Integer rAir ,Integer rGround,String desc).
	 * @brief Constructor with all attributes of the class.
	 * @param String name.
	 * @param Integer a.
	 * @param Integer g.
	 * @param Integer t.
	 * @param Integer rAir.
	 * @param Integer rGround.
	 * @param String desc.
	 */	
	public Club(String name, Integer a, Integer g, Integer t, Integer rAir, Integer rGround, String desc){		
		clubName=name;
		this.air=a;
		this.ground=g;
		this.tee=t;
		this.roughAir=rAir;
		this.roughGround=rGround;		
		this.description=desc;
	}
	
	/**
	 * @fn String toString().
	 * @brief Method to string.
	 * @return String: The club in a String format.
	 */	
	public final String toString(){
		return "Nombre: "+ getClubName()+ "\nAir: " + getAir()+ "\nGround: "+ getGround()+ 
				"\nTee: "+getTee()+"\nRoughAir: "+getRoughAir()+"\nRoughGround: "+getRoughGround()+
				"\nDescription: "+getDescription();
	}
	
	/**
	 * @fn String getDescription().
	 * @brief Getter of the property <tt>description</tt>. 
	 * @return String.
	 */
	public final String getDescription(){
		return description;
	}
	
	/**
	 * @fn Integer getTee().
	 * @brief Getter of the property <tt>tee</tt>. 
	 * @return Integer.
	 */
	public final Integer getTee(){
		return tee;
	}	
	
	/**
	 * @fn Integer getRoughAir().
	 * @brief Getter of the property <tt>RoughAir</tt>. 
	 * @return Integer. 
	 */	
	public final Integer getRoughAir(){
		return roughAir;	
	}	
	
	/**
	 * @fn Integer getRoundGround().
	 * @brief Getter of the property <tt>RoughGround</tt>.
	 * @return Integer.
	 */
	public final Integer getRoughGround(){
		return roughGround;
	}	
	
	/**
	 * @fn String getClubName().
	 * @brief Getter of the property <tt>clubName</tt>.
	 * @return String: name of the club.
	 */	
	public final String getClubName(){
		return clubName;
	}
	
	/**
	 * @fn Integer getAir().
	 * @brief Getter of the property <tt>air</tt>.
	 * @return Integer.
	 */	
	public final Integer getAir(){
		return air;
	}
	
	/**
	 * @fn Integer getGround().
	 * @brief Getter of the property <tt>gorund</tt>.
	 * @return Integer.
	 */
	public final Integer getGround(){
		return ground;
	}					
}
