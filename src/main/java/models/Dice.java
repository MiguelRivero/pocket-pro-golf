package models;

import java.util.Random;

/**
 * @file Dice
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 15/12/2011
 */
public abstract class Dice{

	/**
	 * Attribute with the given number faces.
	 */
	private static final Integer FACES = 6;
	
	/**
	 * Random type attribute.
	 */
	private static Random random = new Random();
	
	/**
	 * @fn Integer roll().
	 * @brief Random dice roll.
	 * @return Integer
	 */
	public static Integer roll(){
		return Math.abs(random.nextInt(FACES)) + 1;
	}
}
