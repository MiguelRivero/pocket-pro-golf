package cards;

import java.awt.Point;
import java.io.IOException;
import java.util.*;
import utilities.*;

/**
 * @file HoleCard.
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 26/10/2011.
 * 
 * Class with constructor methods that model the type HoleCard, referring to a card game type. 
 */
public class HoleCard{
	
	/**
	 * Name of the property holeCard.
	 */
	private final String holeCardName;
	
	/**
	 * Attribute that holds a card of the type HoleCard.The list of lists of strings modeled our surface.
	 */
	private final List<List<String>> holeSegment;
	
	/**
	 * @fn HoleCard(String file).
	 * @brief Constructor of HoleCard with parameters.
	 * @param String file: File where is the cards.
	 * @throws IOException
	 * 
	 * Constructor method of Random card.
	 */
	public HoleCard(String file) throws IOException{		
		List<Object> read=CardReader.readCard(file);
		holeCardName=(String) read.get(0);
		holeSegment =(List<List<String>>) read.get(1);		
	}
	
	/**
	 * @fn HoleCard (String file, String card).
	 * @brief Constructor of HoleCard.
	 * @param String file: file where is the card.
	 * @param String card: card desired.
	 * @throws IOException
	 */
	public HoleCard (String file, String card) throws IOException{
		List<Object> read=CardReader.readCard(file, card);
		holeCardName=(String) read.get(0);
		holeSegment =(List<List<String>>) read.get(1);			
	}
	
	/**
	 * @fn String getSurface(Point coordinates).
	 * @brief Return the surface of the point.
	 * @param Point coordinates: A point to evaluate the surface.
	 * @return String.
	 * 
	 * This method returns the surface where the ball is and throw Exception if the ball goes out of field.
	 */
	public final String getSurface(Point coordinates){		
		String surface="";
		
		if (coordinates == null){
			throw new IllegalArgumentException("Coordinates are null on HoleCard class");
		}else if(coordinates.getY() > getHoleSegment().size() && coordinates.getX() > getHoleSegment().get(coordinates.y).size()){
			throw new OutOfFieldException("You are out of the field");
		}else{
			surface = getHoleSegment().get(coordinates.y).get(coordinates.x);
		}
		
		return surface;
	}
	
	/**
	 * @fn String getHoleCardName().
	 * @brief Getter of the property <tt>HoleCardName</tt> 
	 * @return String.
	 */
	public final String getHoleCardName(){
		return holeCardName;
	}
	
	/**
	 * @fn String toString().
	 * @brief Method toString of HoleCard.
	 * @return String: Returns the HoleCard as a String.
	 */
	public final String toString(){
		//String card = "";
		List<List<String>> aux = getHoleSegment();      
		StringBuffer buffer = new StringBuffer();
		//card += "Card name: " + getHoleCardName() + "\n";
		buffer.append("Card name: " + getHoleCardName() + "\n");
		
		for(int i = aux.size() - 1; i >= 0; i--){
			List<String> list = aux.get(i);
			for(String surface : list){
				//card += String.format("%4s",surface);
				buffer.append(String.format("%4s",surface));
			}
			//card += "\n";
			buffer.append("\n");
		}
		//card += "\n";
		buffer.append("\n");      
		//return card;
		return buffer.toString();
	}
	
	/**
	 * @fn List<List<String>> getHoleSegment().
	 * @brief Getter of the property <tt>HoleSegment</tt>. 
	 * @return List<List<String>> with the holeSegment.
	 */
	public final List<List<String>> getHoleSegment(){
		return holeSegment;
	}
	
	/**
	 * @fn Point getRebound().
	 * @brief Get the rebound of the ball when hit on a tree.
	 * @return Point.
	 * 
	 * Method that returns the position where the ball will bounce after the trees.
	 */
	public final Point getRebound(){
		Point point = new Point();		
		List<List<String>> holeSeg = getHoleSegment();
		Boolean flag = false;
		
		for (int i = 0; i < holeSeg.size() && !flag;i++){
			List<String> line = holeSeg.get(i);
			if (line.contains(Surfaces.getRebound())){
				int x = line.indexOf(Surfaces.getRebound());
				int y = holeSeg.indexOf(line);
				point.setLocation(x, y);
				flag = true;
			}
		}	
		
		return point;
	}	
}
