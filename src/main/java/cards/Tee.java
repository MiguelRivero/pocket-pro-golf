package cards;

import java.awt.Point;
import java.io.IOException;
import java.util.List;
import utilities.Surfaces;

/**
 * @file Tee
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 26/10/2011
 * 
 * This class is a legacy of HoleCard. is passed to the constructors .txt
 * Tee cards type.
 */
public class Tee extends HoleCard{
	
	/**
	 * @fn Tee().
	 * @brief Constructor of Tee.
	 * @throws IOException.
	 */
	public Tee() throws IOException{
		super("files/tee.txt");
	}
	
	/**
	 * @fn Tee(String card).
	 * @brief Costructor of Tee with parameters.
	 * @param String card: Card specific card that the game needs.
	 * @throws IOException.
	 */
	public Tee(String card) throws IOException{
		super("files/tee.txt", card);
	}
	
	/**
	 * @fn Point getStartPoint().
	 * @brief Initial point of the hole.
	 * @return Point: The Start Point.
	 * 
	 * In the card type Tee (start the hole), must have a starting point.
	 * This method return the exact point where begins the hole.
	 */
	public final Point getStartPoint(){
		Point point = new Point();
		List<List<String>> holeSegment = getHoleSegment();
		Boolean flag = false;

		for (int i = 0; i < holeSegment.size() && !flag; i++) {
			List<String> line = holeSegment.get(i);
			if (line.contains(Surfaces.getTee())) {
				int x = line.indexOf(Surfaces.getTee());
				int y = holeSegment.indexOf(line);
				point.setLocation(x, y);
				flag = true;
			}
		}

		return point;
	}
}
