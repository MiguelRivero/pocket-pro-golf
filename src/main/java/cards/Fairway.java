package cards;

import java.io.IOException;

/**
 * @file Fairway.
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 26/10/2011.
 * 
 * This class is a legacy of HoleCard. is passed to the constructors txt Fairway cards type.
 */
public class Fairway extends HoleCard{

	/**
	 * @fn Fairway().
	 * @brief Constructor of fairway.
	 * @throws IOException. 
	 */
	public Fairway() throws IOException{
		super("files/fairway.txt");
	}
	
	/**
	 * @fn Fairway(String card).
	 * @brief Costructor of Fairway with parameters.
	 * @param String card: A fairway card that is select.
	 * @throws IOException.
	 */
	public Fairway(String card) throws IOException{
		super("files/fairway.txt", card);
	}
}
