package cards;

import java.awt.Point;
import java.io.IOException;
import utilities.*;

/**
 * @file Green
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 26/10/2011.
 *
 * This class is a legacy of HoleCard. is passed to the constructors txt Green cards type.
 */
public class Green extends HoleCard{

	/**
	 * @fn Green().
	 * @brief Constructor of Green.
	 * @throws IOException 
	 */
	public Green() throws IOException{
		super("files/green.txt");
	}

	/**
	 * @fn Green(String card).
	 * @brief Constructor of Green with parameters.
	 * @param String card: A green card that is select.
	 * @throws IOException.
	 */
	public Green(String card) throws IOException{
		super("files/green.txt", card);
	}

	/**
	 * @fn Boolean isNextToHole(Point point).
	 * @brief Check if the ball is in the position next to hole. 
	 * @param Point point: the point to check
	 * @return A Boolean that say if next to hole or not.
	 * 
	 * Method that passed a point as a parameter, will tell if the ball is in the surface "Next to Hole".
	 */
	public final Boolean isNextToHole(Point point){
		String surface = "";
			
		if (point == null){
		    throw new IllegalArgumentException("Point is null on Green class");
		}else{
			if(point.getY() > getHoleSegment().size() && point.getX() > getHoleSegment().get(point.y).size()){
				throw new OutOfFieldException("You are out of the field");
			}else{
				surface = getSurface(point);
			}
		}
	    
		return surface.compareToIgnoreCase(Surfaces.getNextToHole()) == 0;	        
     }
}