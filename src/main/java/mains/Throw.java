package mains;

import java.io.IOException;
import java.util.*;
import models.Dice;
import utilities.*;

/**
 * @file Throw.
 * @author Grupo 14, ISG2.
 * @version 0.1
 * @date 20/11/2011.
 * 
 * This class make the player's throw depends of the selected tempo.
 */
public class Throw{
	
	/**
	 * Attributes of differents dices.
	 */
	private Integer dice1, dice2, dice3;
	
	/**
	 * Attributes of the player's progress.
	 */
	private Integer progress;
	
	/**
	 * Attributes that say if the player roll doubles ot triples.
	 */
	private Boolean rollDoubles = false, rollTriples = false;
	
	/**
	 * Values of the green card.
	 */
	private final Map<Integer,Integer> values;
	
	/**
	 * @fn Throw().
	 * @brief Constructor of Throw.
	 */
	public Throw(){
		this.values = new HashMap<Integer, Integer>();
		InputStream fe = new InputStream();
		
		try{
			fe = new InputStream("files/valuesThrow.txt");
		}catch(IOException e){
			e.printStackTrace();
		}
		
		ArrayList<String> lines = new ArrayList<String>();
		for (String line : fe) {
			lines.add(line);
		}
		for(String v:lines){
			ArrayList<String> val = Strings.separateElements(v, ":");
			values.put(Integer.valueOf(val.get(0)),Integer.valueOf(val.get(1)));
		}
		
		dice1 = 0;
		dice2 = 0;
		dice3 = 0; 
		progress = 0;
		rollDoubles = false;
		rollTriples = false;
	}
	
	/**
	 * @fn Integer getDice1().
	 * @brief Getter of the property <tt>dice1</tt> 
	 * @return Integer.
	 */
	public final Integer getDice1(){
		return this.dice1;
	}
	
	/**
	 * @fn Integer getDice2().
	 * @brief Getter of the property <tt>dice2</tt> 
	 * @return Integer.
	 */
	public final Integer getDice2(){
		return this.dice2;
	}
	
	/**
	 * @fn Integer getDice3().
	 * @brief Getter of the property <tt>dice3</tt> 
	 * @return Integer.
	 */
	public final Integer getDice3(){
		return this.dice3;
	}
	
	/**
	 * @fn void setDice1(Integer d1).
	 * @brief Setter of the property <tt>dice1</tt> 
	 * @param Integer d1.
	 */
	public final void setDice1(Integer d1){
		this.dice1 = d1;
	}
	
	/**
	 * @fn void setDice2(Integer d2).
	 * @brief Setter of the property <tt>dice2</tt> 
	 * @param Integer d2.
	 */
	public final void setDice2(Integer d2){
		this.dice2 = d2;
	}
	
	/**
	 * @fn void setDice3(Integer d3).
	 * @brief Setter of the property <tt>dice3</tt> 
	 * @param Integer d3.
	 */
	public final void setDice3(Integer d3){
		this.dice3 = d3;
	}
	
	/**
	 * @fn Integer getProgress().
	 * @brief Getter of the property <tt>progress</tt> 
	 * @return Integer.
	 */
	public final Integer getProgress(){
		return this.progress;
	}
	
	/**
	 * @fn void setProgress(Integer num).
	 * @brief Setter of the property <tt>progress</tt> 
	 * @param Integer num.
	 */
	public void setProgress(Integer num){
		this.progress=num;
	}
	
	/**
	 * @fn Boolean getRollDoubles().
	 * @brief Getter of the property <tt>RollDoubles</tt> 
	 * @return Boolean.
	 */
	public final Boolean getRollDoubles(){
		return this.rollDoubles;
	}
	
	/**
	 * @fn Boolean getRollTriples().
	 * @brief Getter of the property <tt>RollTriples</tt> 
	 * @return Boolean.
	 */
	public final Boolean getRollTriples(){
		return this.rollTriples;
	}
	
	/**
	 * @fn void setRollDoubles().
	 * @brief Setter of the property <tt>RollDoubles</tt> 
	 * @param Boolean rD.
	 */
	public final void setRollDoubles(Boolean rD){
		this.rollDoubles = rD;
	}
	
	/**
	 * @fn void setRollTriples().
	 * @brief Setter of the property <tt>RollTriples</tt> 
	 * @param Boolean rT.
	 */
	public final void setRollTriples(Boolean rT){
		this.rollTriples = rT;
	}
	
	/**
	 * @fn void resetRolls().
	 * @brief Put the attributes rollDoubles and rollTriples to false. 
	 */
	public final void resetRolls(){
		setDice1(0);
		setDice2(0);
		setDice3(0);
		setRollDoubles(false);
		setRollTriples(false);
	}
	
	/**
	 * @fn Map<Integer,Integer> getValues().
	 * @brief Getter of the property <tt>values</tt> 
	 * @return Map<Integer,Integer>.
	 */
	public Map<Integer,Integer> getValues(){
		return values;
	}
	
	/**
	 * @fn void throwDices(Integer swingTempo). 
	 * @brief Select the throw selected by the player.
	 * @param Integer swingTempo.
	 */
	public void throwDices(Integer swingTempo){
		resetRolls();
		if(swingTempo == 0)
			nonThrow();
		else if (swingTempo == 1)
			slowThrow();
		else if (swingTempo == 2)
			normalThrow();
		else
			fastThrow();
	}		
	
	/**
	 * @fn Integer nonThrow().
	 * @brief This method is used when the player don't roll beacuse the ball is in the next to hole position.
	 * @return Integer.
	 */
	public final Integer nonThrow(){
		setProgress(1);
		return getProgress();
	}
	
	/**
	 * @fn Integer slowThrow().
	 * @brief This method is used when the player select one dice.
	 * @return Integer.
	 */
	public final Integer slowThrow(){
		setDice1(Dice.roll());
		setProgress(getValues().get(dice1));		
		return getProgress();
	}
	
	/**
	 * @fn Integer normalThrow().
	 * @brief This method is used when the player select two dices.
	 * @return Integer.
	 */
	public final Integer normalThrow(){
		setDice1(Dice.roll());
		setDice2(Dice.roll());
		
		if(isDouble(getDice1(), getDice2())){
			setRollDoubles(true);
			setProgress(2); 
		}else{
			setProgress( getValues().get(getDice1() + getDice2()) );
		}
		
		return getProgress();
	}
	
	/**
	 * @fn Integer fastThrow().
	 * @brief This method is used when the player select three dices.
	 * @return Integer.
	 */
	public final Integer fastThrow(){
		setDice1(Dice.roll());
		setDice2(Dice.roll());
		setDice3(Dice.roll());	
		
		if(isTriple(getDice1(), getDice2(), getDice3())){
			setRollTriples(true);
			setProgress(2);
		}else if(isDouble(getDice1(), getDice2()) || isDouble(getDice1(), getDice3()) || isDouble(getDice2(), getDice3())){
			setRollDoubles(true);
			setProgress(2);
		}else{
			setProgress( getValues().get(getDice1() + getDice2() + getDice3()) );
		}
		
		return getProgress();
	}
	
	/**
	 * @fn Boolean isDouble(Integer num1, Integer num2).
	 * @brief This method checks if a roll of two dice come up double.
	 * @param Integer num1
	 * @param Integer num1
	 * @return Boolean.
	 */
	public static final Boolean isDouble(Integer num1, Integer num2){
		return num1.equals(num2);				
	}
	
	/**
	 * @fn Boolean isTriple(Integer num1, Integer num2, Integer num3).
	 * @brief This method checks if a roll of three dice come up triple.
	 * @param Integer num1
	 * @param Integer num2
	 * @param Integer num3
	 * @return Boolean.
	 */
	public static final Boolean isTriple(Integer num1, Integer num2, Integer num3){
		return num1.equals(num2) && num2.equals(num3);				
	}	
	
	/**
	 * @fn Boolean isRollEight().
	 * @brief This method checks if a roll out eight without double.
	 * @return Boolean.
	 */
	public Boolean isRollEight(){
		Boolean rollEight = false;
		if (getDice1()+getDice2()+getDice3()==8 && !getRollTriples() && !getRollDoubles()){
			rollEight = true;
		}
		return rollEight;
	}	
}
