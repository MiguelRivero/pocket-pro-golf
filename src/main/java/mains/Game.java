package mains;

import java.awt.Point;
import java.util.*;

import utilities.*;
import models.*;

/**
 * @file Game.
 * @author Grupo 14, ISG2.
 * @version 0.1.
 * @date 2/12/2011.
 */
public class Game {

	/**
	 * Attribute of a list of players.
	 */
	private final List<Player> players;

	/**
	 * Attribute of the current players.
	 */
	private Player currentPlayer;

	/**
	 * Attribute of the tournament.
	 */
	private final Tournament tournament;

	/**
	 * Attribute of the throw.
	 */
	private final Throw strike;

	/**
	 * Attribute of the standings.
	 */
	private final Map<String, Integer> standings;

	/**
	 * Attribute of clubBag.
	 */
	private final ClubBag clubBag;

	/**
	 * Attribute of movement.
	 */
	private Movement movement;

	/**
	 * @fn Game(Tournament tour).
	 * @brief constructor of the class with params.
	 * @param Tournament tour.
	 */
	public Game(Tournament tour) {
		players = tour.getPlayers();
		currentPlayer = Factory.newPlayer("");
		/*
		 * try { currentPlayer = new Player(""); } catch (IOException e) {
		 * e.printStackTrace(); }
		 */
		tournament = tour;
		standings = new HashMap<String, Integer>();
		// Se supone que todos los jugadores se llaman de forma distinta
		for (Player player : players) {
			standings.put(player.getPlayerName(), 0);
		}

		clubBag = Factory.newClubBag();
		strike = Factory.newThrow();
	}

	/**
	 * @fn Point calculateAim(Point direction, Point aim).
	 * @brief Calculate the point that belongs with the aim selected.
	 * @param Point direction.
	 * @param Point aim.
	 * @return Point: A point that shows what is the direction the ball takes.
	 */
	private Point calculateAim(Point direction, Point aim) {
		int x = aim.x;
		int y = aim.y;

		// No tengo en cuenta que la direccion sea (0,0) como resultado de estar
		// en el hoyo,pienso que no se daria ese caso

		if (direction.x == 1 && direction.y == 0) {
			y = -x;
			x = 0;
		} else if (direction.x == -1 && direction.y == 0) {
			y = x;
			x = 0;
		} else if (direction.x == 0 && direction.y == -1) {
			y = 0;
			x = -x;
		}

		return new Point(x, y);
	}

	/**
	 * @fn Player calculateCurrentPlayer(Hole hole).
	 * @brief This method assigns the current player after the order .
	 * @param Hole hole.
	 * @return Player
	 */
	private Player calculateCurrentPlayer(Hole hole) {
		playerOrder(hole);
		return getPlayers().get(0);
	}

	/**
	 * @fn Point calculateDirection(Point from, Point to).
	 * @brief Calculate the direction to be followed by the ball.
	 * @param Point from.
	 * @param Point to.
	 * @return Point: A point with the direction.
	 */
	private Point calculateDirection(Point from, Point to) {
		int x = 0;
		int y = 0;

		int distX = to.x - from.x;
		int distY = to.y - from.y;

		if (Math.abs(distX) > Math.abs(distY)) { // Movimiento en horizontal
			if (distX < 0)// Movimiento izquierda
				x = -1;
			else if (distX > 0)// Movimiento derecha
				x = 1;
		} else if (Math.abs(distX) <= Math.abs(distY) && !(distX == 0 && distY == 0)) { // Movimiento en vertical
			if (distY < 0)// Movimiento abajo
				y = -1;
			else if (distY > 0)// Movimiento arriba
				y = 1;
		}

		// Si x2-x1 == y2-y1 implica que: o bien estan en el hoyo,
		// o la distancia X es = a la distancia Y, por ejemplo, que falte uno a
		// la derecha y uno arriba
		// Por lo tanto, pienso, que en este caso solo tenemos que ir hacia
		// adelante o hacia atrass

		return new Point(x, y);
	}

	/**
	 * @fn ClubBag getClubBag().
	 * @brief Getter of the property <tt>clubBag</tt>
	 * @return ClubBag.
	 */
	private ClubBag getClubBag() {
		return this.clubBag;
	}

	/**
	 * @fn Player getCurrentPlayer().
	 * @brief Getter of the property <tt>currentPlayer</tt>
	 * @return Player.
	 */
	private Player getCurrentPlayer() {
		return this.currentPlayer;
	}

	/**
	 * @fn List<Hole> getHoles().
	 * @brief Returns all the holes.
	 * @return List<Hole>.
	 */
	private List<Hole> getHoles() {
		return getTournament().getHoles();
	}

	/**
	 * @fn Movement getMovement().
	 * @brief Getter of the property <tt>movement</tt>
	 * @return Movement.
	 */
	private Movement getMovement() {
		return this.movement;
	}

	/**
	 * @fn List<Player> getPlayers().
	 * @brief Getter of the property <tt>players</tt>.
	 * @return List<Player>.
	 */
	private final List<Player> getPlayers() {
		return this.players;
	}

	/**
	 * @fn Map<String, Integer> getStandings().
	 * @brief Getter of the property <tt>standings</tt>
	 * @return Map<String, Integer>.
	 */
	private Map<String, Integer> getStandings() {
		return this.standings;
	}

	/**
	 * @fn Throw getStrike().
	 * @brief Getter of the property <tt>strike</tt>
	 * @return Throw.
	 */
	private Throw getStrike() {
		return this.strike;
	}

	/**
	 * @fn Tournament getTournament().
	 * @brief Getter of the property <tt>tournament</tt>
	 * @return Tournament.
	 */
	private Tournament getTournament() {
		return this.tournament;
	}

	/**
	 * @fn Boolean isHoleEnded(Hole hole).
	 * @brief Checks if all players have gotten the ball.
	 * @param Hole hole.
	 * @return Boolean.
	 */
	private Boolean isHoleEnded(Hole hole) {
		// Hay que mirar las distancias al hoyo (o a la lista de Next to Hole)
		// de todos los jugadores.
		// Si todos los jugadores estan a 0 de distancia o estan a 0
		// de distancia de alguna casilla Next to Hole, devuelve true.
		Boolean isTheEnd = true;
		List<Point> nextToHoleList = hole.getNextToHoleList();

		for (int i = 0; i < getPlayers().size() && isTheEnd; i++) {
			Player player = getPlayers().get(i);
			isTheEnd = nextToHoleList.contains(player.getBallPosition());
		}

		return isTheEnd;
	}

	/**
	 * @fn boolean isThisSurface(Hole hole, Point position, String surface).
	 * @brief Check if the surface pased for params is the same of the point on the hole.
	 * @param Hole hole.
	 * @param Point position.
	 * @param String surface.
	 * @return boolean.
	 */
	private boolean isThisSurface(Hole hole, Point position, String surface) {
		return hole.getSurface(position).equals(surface);
	}

	/**
	 * @fn Map<String, Integer> play().
	 * @brief This method call the necessary methods to play.
	 * @return Map<String, Integer>.
	 */
	public Map<String, Integer> play() {
		setSkillToken();
		Integer holeNumber = 1;
		for (Hole hole : getHoles()) {
			resetPlayers(hole); // Reinicia los jugadores y a�ade la posicion inicial
			setMovement(Factory.newMovement(hole));
			System.out.println("\n\n");
			System.out.println("*******************");
			System.out.println("**   Hole: " + String.format("%2d", holeNumber) + "    **");
			System.out.println("*******************");
			startNewHole(hole); // Empieza el hoyo
			updateStandings(hole); // Actualizar clasificaciones
			holeNumber++;
			// Creo que aqui se le deberia dar la posibilidad al jugador de
			// terminar la partida.
		}

		return getStandings();
	}

	/**
	 * @fn void playerOrder(Hole actualHole).
	 * @brief This method puts players in order when someone throws.
	 * @param Hole actualHole.
	 */
	private final void playerOrder(Hole actualHole) {
		Collections.sort(getPlayers(), new PlayerOrderComparator(actualHole.getNextToHoleList(), getStandings()));
	}

	/**
	 * @fn void printCurrentPlayerHole (Hole hole).
	 * @brief Print the current hole.
	 * @param Hole hole.
	 */
	private void printCurrentPlayerHole(Hole hole) {
		Point playerLocation = getCurrentPlayer().getBallPosition();
		// flush();
		System.out.println(hole.toString(playerLocation));
	}

	/**
	 * @fn void printMovement(Hole hole, Point position).
	 * @brief Print the wind, progress, dice1, dice 2 and dice3.
	 * @param Hole hole.
	 * @param Point Position.
	 */
	private void printMovement(Hole hole, Point position) {
		//System.out.println(position);
		System.out.println("Progreso " + getStrike().getProgress());
		System.out.println("\nDado1 " + getStrike().getDice1());
		System.out.println("Dado2 " + getStrike().getDice2());
		System.out.println("Dado3 " + getStrike().getDice3());
		printCurrentPlayerHole(hole);
	}

	/**
	 * @fn void resetPlayers(Hole hole).
	 * @brief This method puts players to default value.
	 * @param Hole hole.
	 */
	private void resetPlayers(Hole hole) {
		for (Player player : getPlayers()) {
			player.resetPlayer();
			player.setBallPosition(hole.getStartPoint());
		}
	}

	/**
	 * @fn void setCurrentPlayer(Player cPlayer).
	 * @brief Setter of the property <tt>currentPlayer</tt>.
	 * @param Player cPlayer.
	 */
	private void setCurrentPlayer(Player cPlayer) {
		this.currentPlayer = cPlayer;
	}

	/**
	 * @fn void setMovement(Movement m).
	 * @brief Setter of the property <tt>movement</tt>.
	 * @param Movement m.
	 */
	private void setMovement(Movement m) {
		this.movement = m;
	}

	/**
	 * @fn void setSkillToken().
	 * @brief Set the skill tokens at start the game.
	 */
	private void setSkillToken() {
		// Colocacion skillTokens iniciales para todos los jugadores
		for (Player p : getPlayers()) {
			for (Integer i = 0; i < 2; i++) {
				String selected = Menu.skillToken(p);
				p.addSkillTokenInClub(selected);
			}
		}
	}

	/**
	 * @fn void startNewHole(Hole hole).
	 * @brief This method plays a hole.
	 * @param Hole hole.
	 */
	private void startNewHole(Hole hole) {
		Boolean theEnd = false;
		while (!theEnd) {
			setCurrentPlayer(calculateCurrentPlayer(hole));
			System.out.println("\nPlayer:\t" + getCurrentPlayer().getPlayerName());
			System.out.println("Skill Tokens:\t" + getCurrentPlayer().skillTokensAmount());
			System.out.println("Hits:\t" + getStandings().get(getCurrentPlayer().getPlayerName()) +"\n");
			printCurrentPlayerHole(hole);
			
			// Selecciona palo
			String clubName = Menu.selectClub(getCurrentPlayer(),hole.getSurface(getCurrentPlayer().getBallPosition()));
			Club club = getClubBag().getClub(clubName);
			
			// Selecciona potencia
			Integer swingTempo = Menu.swingTempo();
			getStrike().throwDices(swingTempo);
			
			// Si sale un 8 y no es dobles, anhadir un SkillToken
			if (getStrike().isRollEight()) {
				String clubSkillToken = "";
				if (clubName.equals(Clubs.getPutter())) {
					clubSkillToken = Clubs.getPutter();
					System.out.println("Skill token added to club: "+ clubSkillToken);
				} else if (clubName.equals(Clubs.getDriver())) {
					clubSkillToken = Clubs.getDriver();
					System.out.println("Skill token added to club: "+ clubSkillToken);
				} else
					clubSkillToken = Menu.skillTokenIron(getCurrentPlayer(),clubName);
				getCurrentPlayer().addSkillTokenInClub(clubSkillToken);
			}
			
			//Selecciona punteria
			Point aim = Menu.aim();
			Point position = getCurrentPlayer().getBallPosition();
			Point previousPosition = (Point) position.clone();
			Point direction = calculateDirection(getCurrentPlayer().getBallPosition(),hole.getNearestHole(getCurrentPlayer()));
			getMovement().setDirection(direction);
			aim = calculateAim(direction, aim);

			Boolean isWater, isSand, isTree = false, isOutOfBounds = false;

			printCurrentPlayerHole(hole);

			// Movimiento de Aire
			for (int i = 0; i < club.getAir() && !isTree && !isOutOfBounds; i++) {
				if (i < 2)
					position = getMovement().calculateFirstAndSecondAirMovement(position, aim, clubName, getStrike());
				else
					position = getMovement().calculateAirMovement(position, clubName, getStrike());

				if (isOutOfBounds = hole.isOutOfBounds(position)){
					position = getMovement().getValidPositionOfOutOfBoundsPenalty(position);
					System.out.println("\nTHE BALL HAVE RUN OUT OF BOUNDS\n");
					System.out.println("The ball has been moved to the nearest valid position\n");
				}
				if (isTree = isThisSurface(hole, position, Surfaces.getTree())){
					position = getMovement().getValidPositionOfTreePenalty(position);
					System.out.println("\nTHE BALL HITS WITH A TREE\n");
					System.out.println("The ball has rebounded\n");
				}

				getCurrentPlayer().setBallPosition(position);
				printMovement(hole, position);
				getStrike().throwDices(swingTempo);
			}

			// printCurrentPlayerHole(hole);

			if (isWater = isThisSurface(hole, position, Surfaces.getWater()))
				position = getMovement().getValidPositionOfWaterPenalty(position);
			isSand = isThisSurface(hole, position,Surfaces.getSand());

			getCurrentPlayer().setBallPosition(position);

			// movimiento de ground
			if (!isTree && !isWater && !isOutOfBounds) {
				for (int i = 0; i < club.getGround() && !isTree && !isWater && !isOutOfBounds; i++) {
					if (isSand) {
						Integer progress = club.getGround() - i;
						position = getMovement().calculateGroundAndSandMovement(position, progress);
					}else
						position = getMovement().calculateGroundMovement(position, clubName, getStrike());

					isSand = isThisSurface(hole, position,Surfaces.getSand());
					
					if (isOutOfBounds = hole.isOutOfBounds(position)){
						position = getMovement().getValidPositionOfOutOfBoundsPenalty(position);
						System.out.println("\nTHE BALL HAS RUN OUT OF BOUNDS\n");
						System.out.println("The ball has been moved to the nearest valid position\n");
					}
					if (isWater = isThisSurface(hole, position,Surfaces.getWater())){
						position = getMovement().getValidPositionOfWaterPenalty(position);
						System.out.println("\nSPLASH!!!! THE BALL IS ON THE WATER\n");
						System.out.println("The ball has been moved to the nearest valid position\n");
					}
					if (isTree = isThisSurface(hole, position,Surfaces.getTree())){
						position = getMovement().getValidPositionOfTreePenalty(position);
						System.out.println("\nTHE BALL HITS WITH A TREE\n");
						System.out.println("The ball has rebounded\n");
					}

					getCurrentPlayer().setBallPosition(position);
					printMovement(hole, position);
					getStrike().throwDices(swingTempo);
				}
				// printCurrentPlayerHole(hole);
			}

			// Si cae en agua, arbol o fuera de bandas, se incrementa en 1 el
			// numero de golpes
			if (isWater || isOutOfBounds || isTree)
				getCurrentPlayer().increaseHit();

			// Si se hace mulligan se vuelve a la posicion anterior
			Integer repeatMovement = Menu.repeatMovement();
			if (repeatMovement == 1){
				if (!hole.isNextToHole(position) && getCurrentPlayer().canMulligan()) {
					for (int i = 0; i < 3; i++) {
						String clubSelectedForRemoveToken = Menu.removeSkillTokenMulligan(getCurrentPlayer());
						getCurrentPlayer().removeTokenInClub(clubSelectedForRemoveToken);
					}
					position = previousPosition;
					getCurrentPlayer().setBallPosition(position);
					printCurrentPlayerHole(hole);
				}else	
					System.out.println("\nNOT ENOUGH TOKENS FOR MULLIGAN\n");
			}
			isWater = false;
			isTree = false;
			isSand = false;
			isOutOfBounds = false;
			getCurrentPlayer().increaseHit();
			theEnd = isHoleEnded(hole);
		}
	}

	/**
	 * @fn void updateStandings(Hole hole).
	 * @brief This method upgrade scores according to the number of strokes.
	 * @param Hole hole.
	 */
	private void updateStandings(Hole hole) {
		for (Player player : getPlayers()) {
			Integer points = player.getHits() - hole.getPar();
			String name = player.getPlayerName();
			Integer previousStanding = getStandings().get(name);
			getStandings().put(name, previousStanding + points);
		}
	}

	/*
	 * No se llama nunca, cuando haga falta pos aki esta private Player
	 * getPlayer (String name){ Player player = null; for (Player p :
	 * getPlayers()) if (p.getPlayerName().equals(name)) player = p; return
	 * player; }
	 */

	/*
	 * private void flush() { //Limpia la consola int numberOfLines = 100; for
	 * (int i = 0; i < numberOfLines; i++) System.out.println(); }
	 */
}