package mains;

import java.awt.Point;
import java.io.*;
import java.util.*;
import utilities.*;
import models.*;

/**
 * @file Menu. 
 * @author Grupo 14, ISG2.
 * @version 0.1.
 * @date 15/12/2011
 * @brief Class with static methods that show on the screen the different menus.
 */
public class Menu{
	
	/**
	 * @fn static Integer principalMenu(). 
	 * @brief Principal Menu Screen. The Player choose New Game, Load Game or Exit Game.
	 * @return Integer: The number that player have selected.
	 */
	public static Integer principalMenu(){		
		System.out.println("\nMain Menu\n" +"-------\n"+
				" 1.- New Game\n"+
				" 2.- Exit\n");
		System.out.print("Select an option: ");
		
		return chooseNumericOption();		
	}
	
	/**
	 * @fn static Integer tournamentSelection(). 
	 * @brief Tournament Menu Screen. Predefined Tournament, Random Tournament or Quick Game.
	 * @return Integer: The number that player have selected.
	 */
	public static Integer tournamentSelection(){		
		System.out.println("\nTournament Selection\n"+"--------------------\n"+
				"1.- Select Tournament\n"+
				"2.- Random Tournament\n"+
				"3.- Quick Game\n"+
				"4.- Exit\n");
		System.out.print("Select an option: ");
		
		return chooseNumericOption();
	}
	
	/**
	 * @fn static Map<Integer,String> predefinedTournamentSelection(). 
	 * @brief Predefined Tournaments Menu: The player select one of the tournaments predefined (professional Tournaments).
	 * @return Map<Integer,String>: The number that player have selected and the name of the tournament.
	 */
	public static Integer predefinedTournamentSelection(List<String> names){		
		Integer index;
		
		System.out.println("\nProfessional Tournament Selection\n"+"---------------------------\n");
		for(Integer i=0; i<names.size(); i++){
			System.out.println((i+1)+".- "+names.get(i)+"\n");
		}
			
		System.out.print("Select an option: ");
		index = chooseNumericOption(); 
		return index;		
	}
	 
	/**
	 * @fn static Integer numberOfPlayersSelection().	 
	 * @brief Select Number of Players Menu Screen. One to four players are valid.
	 * @return Integer: The number that player have selected.
	 */
	public static Integer numberOfPlayersSelection(){
		System.out.print("Select number of players (1 - 4): ");
		return chooseNumericOption();
	}
		 
	/**	 
	 * @fn static String inputPlayer(). 
	 * @brief Input Player Menu Screen.
	 * @return String: The name of a player, typed by game user.
	 */
	public static String inputPlayer(){
		System.out.print("\nType Player name: ");
		return typeStringsValues();
	}
	 
	/**
	 * @fn static String skillToken(Player player).
	 * @brief For each player shows the types of club,the player chooses where to put their "skill tokens".
	 * @param Player player.
	 * @return String: The name of the club where the game user wants to put a skill token.
	 */
	public static String skillToken(Player player){
		System.out.println("Skill Token\n"+"---------------\n");
		ClubBag clubBag = Factory.newClubBag();
		System.out.println(clubBag.toString(player));
		System.out.println(player.getPlayerName()+", please select a Club to set an Skill Token: \n");
		
		Integer selection = chooseNumericOption();
		String ret="";		
			
		if(selection == 1){
			ret= Clubs.getWedge();
		}else if(selection == 2){
			ret= Clubs.getDriver();
		}else if(selection == 3){
			ret= Clubs.getPutter();
		}else if(selection == 4){
			ret= Clubs.getIron3();
		}else if(selection == 5){
			ret= Clubs.getIron5();
		}else if(selection == 6){
			ret= Clubs.getIron7();
		}else if(selection == 7){
			ret= Clubs.getIron9();
		}else{
			ret=skillToken(player);
		}
		
		return ret;
	}
	
	/**
	 * @fn static String removeSkillTokenMulligan(Player player).
	 * @brief When a player wants to do Mulligan, the player may select the clubs for removing skilltokens.
	 * @param Player player.
	 * @return String: The name of the club to remove a skilltoken.
	 */
	public static String removeSkillTokenMulligan(Player player){
		System.out.println("Skill Token for Mulligan\n"+"---------------\n");
		ClubBag clubBag = Factory.newClubBag();
		System.out.println(clubBag.toString(player));
		System.out.println(player.getPlayerName()+", please select a Club to remove a Skill Token: \n");
		
		Integer selection = chooseNumericOption();
		String ret="";		
			
		if(selection == 1){
			ret= Clubs.getWedge();
		}else if(selection == 2){
			ret= Clubs.getDriver();
		}else if(selection == 3){
			ret= Clubs.getPutter();
		}else if(selection == 4){
			ret= Clubs.getIron3();
		}else if(selection == 5){
			ret= Clubs.getIron5();
		}else if(selection == 6){
			ret= Clubs.getIron7();
		}else if(selection == 7){
			ret= Clubs.getIron9();
		}else{
			ret=skillToken(player);
		}
		
		return ret;
	}
	
	/**
	 * @fn static String skillTokenIron(Player currentPlayer, String clubUsed).
	 * @brief When a player have to put an skill token because a roll result is "8", can select an Iron club (if have striked when an Iron club)
	 * @param Player currentPlayer.
	 * @param String clubUsed
	 * @return String: The name of the club where the game user wants to put a skill token.
	 */
	public static String skillTokenIron(Player currentPlayer, String clubUsed){
		System.out.println("YOU GAIN A SKILL TOKEN!!!\n"+"--------------------\n");
		ClubBag bag = Factory.newClubBag();
		String ret="";
		if("Wedge".equals(clubUsed)){
			System.out.println("1.- "+clubUsed+": "
					+bag.getClub(clubUsed).getDescription()+"\n"+"    Skill: "
					+currentPlayer.getSkillTokenInClub(clubUsed)
					+"\n\n"+"2.- "+Clubs.getIron9()+": "
					+bag.getClub(Clubs.getIron9()).getDescription()+"\n"+"    Skill: "
					+currentPlayer.getSkillTokenInClub(Clubs.getIron9())
					+"\n\n");
			System.out.println(currentPlayer.getPlayerName()+", please select a Club to set an Skill Token: \n");
			
			Integer selection = chooseNumericOption();
			
			if(selection == 1){
				ret= Clubs.getWedge();
			}else if(selection == 2){
				ret= Clubs.getIron9();
			}else{
				ret=skillTokenIron(currentPlayer, clubUsed);
			}
		}
		
		if("3Iron".equals(clubUsed)){
			System.out.println("1.- "+clubUsed+": "
					+bag.getClub(clubUsed).getDescription()+"\n"+"    Skill: "
					+currentPlayer.getSkillTokenInClub(clubUsed)
					+"\n\n"+"2.- "+Clubs.getIron5()+": "
					+bag.getClub(Clubs.getIron5()).getDescription()+"\n"+"    Skill: "
					+currentPlayer.getSkillTokenInClub(Clubs.getIron9())
					+"\n\n");
			System.out.println(currentPlayer.getPlayerName()+", please select a Club to set an Skill Token: \n");
			
			Integer selection = chooseNumericOption();
		
			if(selection == 1){
				ret= Clubs.getIron3();
			}else if(selection == 2){
				ret= Clubs.getIron5();
			}else{
				ret=skillTokenIron(currentPlayer, clubUsed);
			}
		}
		
		if("5Iron".equals(clubUsed)){
			System.out.println("1.- "+Clubs.getIron3()+": "
					+bag.getClub(Clubs.getIron3()).getDescription()+"\n"+"    Skill: "
					+currentPlayer.getSkillTokenInClub(Clubs.getIron9())
					+"\n\n"+"2.- "+clubUsed+": "
					+bag.getClub(clubUsed).getDescription()+"\n"+"    Skill: "
					+currentPlayer.getSkillTokenInClub(clubUsed)
					+"\n\n"+"3.- "+Clubs.getIron7()+": "
					+bag.getClub(Clubs.getIron7()).getDescription()+"\n"+"    Skill: "
					+currentPlayer.getSkillTokenInClub(Clubs.getIron9())
					+"\n\n");
			System.out.println(currentPlayer.getPlayerName()+", please select a Club to set an Skill Token: \n");
			
			Integer selection = chooseNumericOption();
		
			if(selection == 1){
				ret= Clubs.getIron3();
			}else if(selection == 2){
				ret= Clubs.getIron5();
			}else{
				ret=skillTokenIron(currentPlayer, clubUsed);
			}
		}
		
		if("7Iron".equals(clubUsed)){
			System.out.println("1.- "+Clubs.getIron5()+": "
					+bag.getClub(Clubs.getIron5()).getDescription()+"\n"+"    Skill: "
					+currentPlayer.getSkillTokenInClub(Clubs.getIron9())
					+"\n\n"+"2.- "+clubUsed+": "
					+bag.getClub(clubUsed).getDescription()+"\n"+"    Skill: "
					+currentPlayer.getSkillTokenInClub(clubUsed)
					+"\n\n"+"3.- "+Clubs.getIron9()+": "
					+bag.getClub(Clubs.getIron9()).getDescription()+"\n"+"    Skill: "
					+currentPlayer.getSkillTokenInClub(Clubs.getIron9())
					+"\n\n");
			System.out.println(currentPlayer.getPlayerName()+", please select a Club to set an Skill Token: \n");
			
			Integer selection = chooseNumericOption();
		
			if(selection == 1){
				ret= Clubs.getIron3();
			}else if(selection == 2){
				ret= Clubs.getIron5();
			}else{
				ret=skillTokenIron(currentPlayer, clubUsed);
			}
		}
		
		if("9Iron".equals(clubUsed)){
			System.out.println("1.- "+Clubs.getIron7()+": "
					+bag.getClub(Clubs.getIron7()).getDescription()+"\n"+"    Skill: "
					+currentPlayer.getSkillTokenInClub(Clubs.getIron9())
					+"\n\n"+"2.- "+clubUsed+": "
					+bag.getClub(clubUsed).getDescription()+"\n"+"    Skill: "
					+currentPlayer.getSkillTokenInClub(clubUsed)
					+"\n\n"+"3.- "+Clubs.getWedge()+": "
					+bag.getClub(Clubs.getWedge()).getDescription()+"\n"+"    Skill: "
					+currentPlayer.getSkillTokenInClub(Clubs.getIron9())
					+"\n\n");
			System.out.println(currentPlayer.getPlayerName()+", please select a Club to set an Skill Token: \n");
			
			Integer selection = chooseNumericOption();
		
			if(selection == 1){
				ret= Clubs.getIron3();
			}else if(selection == 2){
				ret= Clubs.getIron5();
			}else{
				ret=skillTokenIron(currentPlayer, clubUsed);
			}
		}
		
		return ret;
	}
	
	/**
	 * @fn static void swingTempo(). 
	 * @brief Select Tempo: Slow, Normal or Fast.
	 * @return Integer: The number that player have selected.
	 */
	public static Integer swingTempo(){		
		System.out.println("Swing Tempo\n"+"------------\n"+
				"1.- Slow\n"+
				"2.- Normal\n"+
				"3.- Fast\n");
		System.out.print("\nSelect tempo to strike the ball: ");
		
		List<Integer> validValues = new ArrayList<Integer>();
		Integer ret=chooseNumericOption();
		
		for(Integer add = 1; add<4; add++){
			validValues.add(add);
		}
		if(!validValues.contains(ret)){
			ret=swingTempo();
		}
		
		return ret;
	}
	
	/**
	 * @fn static void repeatMovement(). 
	 * @brief Movement Menu, Use Skill Token, Mulligan or Continue.
	 * @return Integer: The number that player have selected.
	 */
	public static Integer repeatMovement(){		
		System.out.println("Repeat Movement\n"+"---------------\n"
				+"1.- Mulligan\n"
				+"2.- Continue\n");
		System.out.print("\nUse Skill Token hability? Choose: ");
		
		Integer ret=0;
		Integer selection=chooseNumericOption();
		
		if(selection==1){
			ret=1;
		}else if(selection==2){
			ret=2;
		}else{
			repeatMovement();
		}
		
		return ret;
	}
	
	/**
	 * @fn static Point aim(). 
	 * @brief Aim Menu. Player choose Top Draw, Draw, Center, Fade or Top Fade. 
	 * @return Point: A point that shows what is the direction the ball takes.
	 */
	public static Point aim(){		
		System.out.println("Aim\n"+"-----\n"+
				"1.- Top Draw\n"+
				"2.- Draw\n"+
				"3.- Center\n"+
				"4.- Fade\n"+
				"5.- Top Fade\n");
		System.out.print("\nSelect aim to strike the ball: ");		
		
		Point aim = new Point(0, 0);		
		Integer selection = chooseNumericOption();
		
		if(selection == 1){
			aim.setLocation(-2, 0);
		}else if(selection == 2){
			aim.setLocation(-1,0);		
		}else if(selection == 3){
			aim.setLocation(0,0);
		}else if(selection == 4){
			aim.setLocation(1,0);
		}else if(selection == 5){
			aim.setLocation(2,0);
		}else{
			aim();
		}
		
		return aim;
	}	
	
	/**
	 * @fn static String selectClub(Player player, String surface).
	 * @brief Returns the club selected by the player.
	 * @param Player player: The player that is selecting the club.
	 * @param String surface: The surface of the player's ball position.
	 * @return String: The name of the selected club.
	 * 
	 * This method is used when the user of UI have to type a String value, for example, a player name or the name of a Club for selecting it.
	 */
	public static String selectClub(Player player, String surface){		
		ClubBag clubBag = Factory.newClubBag();
		List<Club> valids = new ArrayList<Club>();

		String imIn = "";
		imIn = surface;
		
		String ret2 = "";
		
		System.out.println("Select Club\n"+"---------------\n");
		
		if(imIn.equals(Surfaces.getFairway())||imIn.equals(Surfaces.getRough())||imIn.equals(Surfaces.getTee())){
			System.out.println(clubBag.toString(player)+"\n"+player.getPlayerName()+", Select a Club: ");			
			Integer selection = chooseNumericOption();					
				
			if(selection == 1){
				ret2= Clubs.getWedge();
			}else if(selection == 2){
				ret2= Clubs.getDriver();
			}else if(selection == 3){
				ret2= Clubs.getPutter();
			}else if(selection == 4){
				ret2= Clubs.getIron3();
			}else if(selection == 5){
				ret2= Clubs.getIron5();
			}else if(selection == 6){
				ret2= Clubs.getIron7();
			}else if(selection == 7){
				ret2= Clubs.getIron9();
			}else{
				ret2=selectClub(player,surface);
			}			
		}else if(imIn.equals(Surfaces.getSand())){			
			valids.add(clubBag.getClub(Clubs.getIron3()));
			valids.add(clubBag.getClub(Clubs.getIron5()));
			valids.add(clubBag.getClub(Clubs.getIron7()));
			valids.add(clubBag.getClub(Clubs.getIron9()));
			valids.add(clubBag.getClub(Clubs.getWedge()));
			
			StringBuffer ret = new StringBuffer();
			
			Integer i=1;
			for(Club c:valids){
				ret.append(i+".- "+c.getClubName()+": "
						+c.getDescription()+"\n"+"    Skill: "
						+player.getSkillTokenInClub(c.getClubName())+"\n\n");
				i++;
			}
			System.out.println(ret);
			valids.clear();
			
			System.out.println(player.getPlayerName()+", please select a Club to strike the ball: \n");
			
			Integer selection = chooseNumericOption();
			
			List<Integer> possibleAnswers=new ArrayList<Integer>();
			for(Integer answer=1; answer<6; answer++){
				possibleAnswers.add(answer);
			}			
				
			if(selection == 1){
				ret2= Clubs.getIron3();
			}else if(selection == 2){
				ret2= Clubs.getIron5();
			}else if(selection == 3){
				ret2= Clubs.getIron7();
			}else if(selection == 4){
				ret2= Clubs.getIron9();
			}else if(selection == 5){
				ret2= Clubs.getWedge();
			}else if(!possibleAnswers.contains(selection)){
				ret2=selectClub(player, surface);
			}		
		}else if(imIn.equals(Surfaces.getNextToHole())||imIn.equals(Surfaces.getGreen())){			
			System.out.println("You are on the green and _Putter_ is auto-selected.");
			ret2 = Clubs.getPutter();			
		}
		
		return ret2;
	}

	/**
	 * @fn static Integer chooseNumericOption().
	 * @brief Capture keyboard input.
	 * @return Integer.
	 * 
	 * This method is used when the user of UI have to select an option typing a Integer number between an options.
	 */
	public static Integer chooseNumericOption(){
		Integer param;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String linea = "";
		
		try{
			linea = br.readLine();
		}catch(IOException e){
			e.printStackTrace();
		}
		
		try{
			param = Integer.parseInt(linea);
		}catch(NumberFormatException e){
			param = -1;
		}
		
		return param;		
	}

	/**
	 * @fn static String typeStringsValues().
	 * @brief Check keyboard input.
	 * @return String: The String typed by game user. 
	 * 
	 * This method is used when the user of UI have to type a String value, for example, a player name or the name of a Club for selecting it.
	 */
	public static String typeStringsValues(){
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String linea = "Cadena no valida.";
		
		try{
			linea = br.readLine();
		}catch(IOException e){
			e.printStackTrace();
		}
		
		return linea;
	}
}
