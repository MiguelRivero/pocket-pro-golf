package mains;

import java.io.IOException;
import java.util.*;

import models.*;
import utilities.*;

/**
 * @file Main.
 * @author Grupo 14, ISG2.
 * @version 0.1.
 * @date 21/10/2011.
 * 
 * This class make an interaction between the game and the users with the objective of taking the necessary information
 * for the Game.java class at the beginning of a game.
 */
public class Main{	
	
	public static void main (String []args) throws IOException{	
		Integer mainMenuAnswer = -1;
		Integer tournamentSelectionAnswer = -1;
		Integer playersCount = -1;
		String name = "";
		String gameType = "Quick Game";
		List<Player> players = new ArrayList<Player>();
		List<Hole> holes = new ArrayList<Hole>();		
		Tournament tour;
		
		while (mainMenuAnswer == -1){
			mainMenuAnswer = Menu.principalMenu();
			if(mainMenuAnswer>2 || mainMenuAnswer<1){
				System.out.println("\n____Invalid option.____\n");
				mainMenuAnswer = -1;
			}
		}
		
		if (mainMenuAnswer != 2){
			if(mainMenuAnswer == 1){
				while (tournamentSelectionAnswer == -1){
					tournamentSelectionAnswer = Menu.tournamentSelection();
					if(tournamentSelectionAnswer>4 || tournamentSelectionAnswer<1){
						System.out.println("\n____Invalid option.____\n");
						tournamentSelectionAnswer=-1;
					}
				}
				if (tournamentSelectionAnswer != 4){
					if(tournamentSelectionAnswer == 1){			
						List<String> tournamentsList = TournamentReader.readTournamentNames("files/tournaments.txt");
						Integer index =Menu.predefinedTournamentSelection(tournamentsList);
						gameType = tournamentsList.get(index-1);
						holes = TournamentReader.readTournament("files/tournaments.txt", gameType);			
					}
					if(tournamentSelectionAnswer == 2){
						gameType = "Random Tournament";
						for(Integer i=1; i<=18; i++){
							try{
								holes.add(Factory.newHole(new Random().nextInt(6)));
							}catch(IOException e){
								e.printStackTrace();
							}
						}	
					}
					if(tournamentSelectionAnswer == 3){
						try{
							holes.add(Factory.newHole(new Random().nextInt(6)));
						}catch(IOException e){
							e.printStackTrace();
						}
					}
					
					while (playersCount==-1){
						playersCount = Menu.numberOfPlayersSelection();
						if(playersCount>4 || playersCount<1){
							System.out.println("\n____Invalid option.____\n");
							playersCount=-1;
						}else{
							for(Integer i=playersCount; i>0; i--){
								name = Menu.inputPlayer();
								players.add(Factory.newPlayer(name));
							}
						}
					}
					
				
					tour = Factory.newTournament(gameType, players, holes);
					System.out.println(tour);
					Game game = Factory.newGame(tour);
					Map<String, Integer> standings = game.play();
					showStandings(standings);
				}
			}
		}
		System.out.println("\n\n");
		System.out.println("*******************");
		System.out.println("**    The End    **");
		System.out.println("*******************");
	}	
	
	/**
	 * @fn static void showStandings(Map<String, Integer> standings).
	 * @brief Show the standings.
	 * @param Map<String, Integer> standings
	 */
	private static void showStandings(Map<String, Integer> standings) {
		Map<Integer, String> clasification = switchStandings(standings);
		System.out.println("Jugador\tGolpes");
		for (Integer hit : clasification.keySet())
			System.out.println( clasification.get(hit) + "\t" + hit);
	}

	/**
	 * @fn static Map<Integer, String> switchStandings(Map<String, Integer> standings).
	 * @brief This method help to create the standing.
	 * @param Map<String, Integer> standings.
	 * @return Map<Integer, String>.
	 */
	private static Map<Integer, String> switchStandings(Map<String, Integer> standings) {
		Integer[] minMax = getMinMaxStanding(standings);
		Map <Integer, String> clasification = new HashMap<Integer, String>();

		for (int i = minMax[1]; i >= minMax[0]; i--)
			clasification.put(i, "");
		
		for (String player : standings.keySet())
			clasification.put(standings.get(player), player);
		
		return clasification;
	}
	
	/**
	 * @fn static Integer[] getMinMaxStanding(Map<String, Integer> standings).
	 * @brief This method return the minimun and maximun standing of all players.
	 * @param Map<String, Integer> standings.
	 * @return Integer[].
	 */
	private static  Integer[] getMinMaxStanding(Map<String, Integer> standings){
		Integer max = 0, min = 0;
		for (String player : standings.keySet()){
			Integer playerHits = standings.get(player);
			if (max < playerHits)
				max = playerHits;
			if (min > playerHits)
				min = playerHits;
		}

		Integer[] minMax = {max, min};		
		return minMax;
	}
}

